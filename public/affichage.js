// fonction qui s'occupe l'affichage graphique du jeux
function showMap(world) {
    image(texture.fond, 0, 0, pageX, gameY);
    for (var y = world.camera.posY; y < gameY / cube + world.camera.posY; y++) {
      for (var x = world.camera.posX; x <= world.camera.posX + Math.floor(pageX / cube); x++) {
        try {
          if ((world.dim[currentDim].map[y][x] == " " || objet[world.dim[currentDim].map[y][x]].transparent) && objet[world.dim[currentDim].deco[y][x]].texturef != undefined) {
            if(objet[world.dim[currentDim].deco[y][x]].canFall){
              if (world.dim[currentDim].map[y + 1][x] == " ") {
                if(objet[world.dim[currentDim].deco[y][x]].loot != " "){
                  createProjectile(world.dim[currentDim], x * cube, y * cube, 0, 0, objet[world.dim[currentDim].deco[y][x]].loot, 1);
                }
                world.dim[currentDim].deco[y][x] = " ";
                world.dim[currentDim].dur[y][x] = 0;
              } else {
                if (objet[world.dim[currentDim].deco[y][x]].texturef != "none") {
                  image(objet[world.dim[currentDim].deco[y][x]].texturef, x * cube - world.camera.posX * cube, y * cube - world.camera.posY * cube, cube, cube);
                }
                else {
                  image(objet[world.dim[currentDim].deco[y][x]].texture, x * cube - world.camera.posX * cube, y * cube - world.camera.posY * cube, cube, cube);
                  fill("rgba(0,0,0,0.25)");
                  rect(x * cube - world.camera.posX * cube, y * cube - world.camera.posY * cube, cube, cube);
                }
              }
            }
            else {
              if (objet[world.dim[currentDim].deco[y][x]].texturef != "none") {
                image(objet[world.dim[currentDim].deco[y][x]].texturef, x * cube - world.camera.posX * cube, y * cube - world.camera.posY * cube, cube, cube);
              }
              else {
                image(objet[world.dim[currentDim].deco[y][x]].texture, x * cube - world.camera.posX * cube, y * cube - world.camera.posY * cube, cube, cube);
                fill("rgba(0,0,0,0.25)");
                rect(x * cube - world.camera.posX * cube, y * cube - world.camera.posY * cube, cube, cube);
              }
            }
          }
  
          
          if(objet[world.dim[currentDim].map[y][x]].texture != undefined){
            if(objet[world.dim[currentDim].map[y][x]].canFall){
              if (world.dim[currentDim].map[y + 1][x] == " ") {
                console.log(objet[world.dim[currentDim].map[y][x]]);
                createFallingBlock(world.dim[currentDim], x, y, world.dim[currentDim].map[y][x]);
                world.dim[currentDim].map[y][x] = " ";
                if (world.dim[currentDim].deco[y][x] != " ") {
                  world.dim[currentDim].dur[y][x] = objet[world.dim[currentDim].deco[y][x]].dur;
                }
                else {
                  world.dim[currentDim].dur[y][x] = 0;
                }
              } else {
                image(objet[world.dim[currentDim].map[y][x]].texture, x * cube - world.camera.posX * cube, y * cube - world.camera.posY * cube, cube, cube);
              }
            }
            else {
              image(objet[world.dim[currentDim].map[y][x]].texture, x * cube - world.camera.posX * cube, y * cube - world.camera.posY * cube, cube, cube);
            }
          }
  
          if ((world.dim[currentDim].map[y][x] != " " && world.dim[currentDim].dur[y][x] < objet[world.dim[currentDim].map[y][x]].dur)) {
            fill("red");
            text(world.dim[currentDim].dur[y][x], x * cube - world.camera.posX * cube, y * cube - world.camera.posY * cube + cube);
            world.dim[currentDim].dur[y][x]++;
          } else if (world.dim[currentDim].deco[y][x] != " " && world.dim[currentDim].map[y][x] == " " && world.dim[currentDim].dur[y][x] < objet[world.dim[currentDim].deco[y][x]].dur) {
            fill("blue");
            text(world.dim[currentDim].dur[y][x], x * cube - world.camera.posX * cube, y * cube - world.camera.posY * cube + cube);
            world.dim[currentDim].dur[y][x]++;
          }
        } catch (err) {
          // charge la carte si erreur
          if(x > 0){
            loadMap(world, 500, "est");
          }
          else if(x < 0){
            loadMap(world, 500, "ouest");
          }
          x--; // re-affiche le bloc erreur
        }
      }
    }
  
  
    for (var i = 0; i < world.dim[currentDim].sable.length; i++) {
      image(objet[world.dim[currentDim].sable[i].o].texture, world.dim[currentDim].sable[i].posX - world.camera.posX * cube, world.dim[currentDim].sable[i].posY - world.camera.posY * cube, cube, cube);
    }
  
    for (var i = 0; i < world.dim[currentDim].project.length; i++) {
      image(objet[world.dim[currentDim].project[i].o].texture, world.dim[currentDim].project[i].posX - world.camera.posX * cube - cube / 4, world.dim[currentDim].project[i].posY - world.camera.posY * cube - cube / 4, cube / 2, cube / 2);
    }
  
  
    // Selecteur
    if (Math.sqrt(Math.pow(Math.abs(sourisX - (world.player.posX + Math.floor(world.player.taille / 2) - world.camera.posX * cube)), 2) + Math.pow(Math.abs(sourisY - (world.player.posY + Math.floor(world.player.taille / 2) - world.camera.posY * cube)), 2)) < world.player.range || world.player.range == "infini") {
      if (ctrl == false) {
        if (sourisgauche || sourisdroite) {
          image(texture.selecteur.blancT, Math.floor(sourisX / cube) * cube, Math.floor(sourisY / cube) * cube, cube, cube);
        } else {
          image(texture.selecteur.blanc, Math.floor(sourisX / cube) * cube, Math.floor(sourisY / cube) * cube, cube, cube);
        }
      } else {
        if (sourisgauche || sourisdroite) {
          image(texture.selecteur.vertT, Math.floor(sourisX / cube) * cube, Math.floor(sourisY / cube) * cube, cube, cube);
        } else {
          image(texture.selecteur.vert, Math.floor(sourisX / cube) * cube, Math.floor(sourisY / cube) * cube, cube, cube);
        }
      }
    } else {
      image(texture.selecteur.invalide, Math.floor(sourisX / cube) * cube, Math.floor(sourisY / cube) * cube, cube, cube);
    }
    // player
    if (world.player.accelerationY > 0) {
      if (world.player.facing == "est") {
        image(texture.player.SD, world.player.posX - world.camera.posX * cube, world.player.posY - world.camera.posY * cube, world.player.taille, world.player.taille);
      } else {
        image(texture.player.SG, world.player.posX - world.camera.posX * cube, world.player.posY - world.camera.posY * cube, world.player.taille, world.player.taille);
      }
    } else if (shift == true || bas == true) {
      if (world.player.facing == "est") {
        image(texture.player.GD, world.player.posX - world.camera.posX * cube, world.player.posY - world.camera.posY * cube, world.player.taille, world.player.taille);
      } else {
        image(texture.player.GG, world.player.posX - world.camera.posX * cube, world.player.posY - world.camera.posY * cube, world.player.taille, world.player.taille);
      }
    } else if (gauche == true || droite == true) {
      if (world.player.facing == "est") {
        if (tick - Math.floor(tick / repet) * repet < (repet / 2)) {
          // taille ajustée manuellement car player plus petit sur cette image
          image(texture.player.M1D, world.player.posX - world.camera.posX * cube - world.player.taille * 0.05, world.player.posY - world.camera.posY * cube - world.player.taille * 0.05, world.player.taille * 1.1, world.player.taille * 1.05);
        } else {
          image(texture.player.M2D, world.player.posX - world.camera.posX * cube, world.player.posY - world.camera.posY * cube, world.player.taille, world.player.taille);
        }
      } else {
        if (tick - Math.floor(tick / repet) * repet < (repet / 2)) {
          // taille ajustée manuellement car player plus petit sur cette image
          image(texture.player.M1G, world.player.posX - world.camera.posX * cube - world.player.taille * 0.05, world.player.posY - world.camera.posY * cube - world.player.taille * 0.05, world.player.taille * 1.1, world.player.taille * 1.05);
        } else {
          image(texture.player.M2G, world.player.posX - world.camera.posX * cube, world.player.posY - world.camera.posY * cube, world.player.taille, world.player.taille);
        }
      }
    } else {
      if (world.player.facing == "est") {
        image(texture.player.BD, world.player.posX - world.camera.posX * cube, world.player.posY - world.camera.posY * cube, world.player.taille, world.player.taille);
      } else {
        image(texture.player.BG, world.player.posX - world.camera.posX * cube, world.player.posY - world.camera.posY * cube, world.player.taille, world.player.taille);
      }
    }
    fill("rgb(200,200,200)");
    rect(0, gameY, pageX, pageY - gameY);
    
    //document.title = Math.floor(player.posX / cube) + " | " + Math.floor(player.posY / cube) + " | " + (Math.floor(sourisX / cube) + camera.posX) + " | " + (Math.floor(sourisY / cube) + camera.posY) + " | " + camera.posX + " | " + camera.posY;
    
  }

  function showInfo(world){
    fill("black");
    text("FPS: " + Math.floor(frameRate()) ,10, 20);
    text("Player:",10, 40);
    text("posX: " + Math.floor(world.player.posX / cube),20, 55);
    text("posY: " + Math.floor(world.player.posY / cube),20, 70);
    text("accelerationX: " + world.player.accelerationX,20, 85);
    text("accelerationY: " + world.player.accelerationY,20, 100);
    text("biome: " + biome[world.dim[currentDim].B[Math.floor(world.player.posX / cube)]].name + "(" + world.dim[currentDim].B[Math.floor(world.player.posX / cube)] + ")",20, 115);
    text("Souris:",10, 135);
    text("posX: " + (Math.floor(sourisX / cube) + world.camera.posX),20, 150);
    text("posY: " + (Math.floor(sourisY / cube) + world.camera.posY),20, 165);
    text("biome: " + biome[world.dim[currentDim].B[Math.floor(sourisX / cube) + world.camera.posX]].name + "(" + world.dim[currentDim].B[Math.floor(sourisX / cube) + world.camera.posX] + ")",20, 180);
    text("Camera:",10, 200);
    text("posX: " + world.camera.posX ,20, 215);
    text("posY: " + world.camera.posY ,20, 230);
  }

// affichage graphique des boutons
  function showButtons(){
    fill("black");
    for(var i = 0; i< bouton.length; i++){
      if(bouton[i].f != "none" && bouton[i].f2 != "none"){
        if(sourisX > bouton[i].posX && sourisX <= bouton[i].posX+bouton[i].l && sourisY > bouton[i].posY && sourisY <= bouton[i].posY+bouton[i].h){
          image(bouton[i].f2, bouton[i].posX, bouton[i].posY, bouton[i].l, bouton[i].h);
        }
        else{
          image(bouton[i].f, bouton[i].posX, bouton[i].posY, bouton[i].l, bouton[i].h);
        }
      }
      if(typeof bouton[i].i != "string"){
        if(bouton[i].l >= bouton[i].h){
          image(bouton[i].i,bouton[i].posX+bouton[i].l/2-(bouton[i].h*bouton[i].p1)/2, bouton[i].posY+bouton[i].h/2-(bouton[i].h*bouton[i].p1)/2, bouton[i].h*bouton[i].p1, bouton[i].h*bouton[i].p1);
        }
        else{
          image(bouton[i].i,bouton[i].posX+bouton[i].l/2-(bouton[i].l*bouton[i].p1)/2, bouton[i].posY+bouton[i].h/2-(bouton[i].l*bouton[i].p1)/2, bouton[i].l*bouton[i].p1, bouton[i].l*bouton[i].p1);
        }
      }
      else if (bouton[i].i != "none"){
        text(bouton[i].i, bouton[i].posX+bouton[i].p1,bouton[i].posY+bouton[i].p2);
      }
    }
  }


// affichage graphique de l'inventaire
function showSlots(world) {
    fill("black");
    // barre d'inventaire
    image(texture.inventaire.barre, 0, gameY, 495, pageY - gameY);
    //inventaire ouvert
    if (typeInventaire > 0) {
      image(texture.inventaire.barre, 0, gameY - 70 * 3, 495, 200);
    }
    // petite ou grande table de craft
    if(typeInventaire == 1){
      image(texture.inventaire.craft, 540, gameY - 70 * 3 +10, 250, 180);
    }
    else if(typeInventaire == 2){
      image(texture.inventaire.craft, 540, gameY - 70 * 3 +10, 250, 180);
    }
    for (var i = 0; i < localWorld[currentWorld].player.slot.length; i++) {
      if (i < tailleInventaire[typeInventaire]) {
        if (world.player.slot[i].f != "none" && world.player.slot[i].f2 != "none") { // slot
          if (world.player.slot[i].select != true) {
            image(world.player.slot[i].f, world.player.slot[i].posX, world.player.slot[i].posY, world.player.slot[i].l, world.player.slot[i].h);
          } else {
            image(world.player.slot[i].f2, world.player.slot[i].posX, world.player.slot[i].posY, world.player.slot[i].l, world.player.slot[i].h);
          }
        }
        if (((sourisX == selectX && sourisY == selectY) || world.player.slot[i].click == false) && objet[world.player.slot[i].o].texture != undefined) { // slot occupé
          if (world.player.slot[i].l >= world.player.slot[i].h) {
            image(objet[world.player.slot[i].o].texture, world.player.slot[i].posX + world.player.slot[i].l / 2 - (world.player.slot[i].h * world.player.slot[i].p) / 2, world.player.slot[i].posY + world.player.slot[i].h / 2 - (world.player.slot[i].h * world.player.slot[i].p) / 2, world.player.slot[i].h * world.player.slot[i].p, world.player.slot[i].h * world.player.slot[i].p);
            text(world.player.slot[i].q, world.player.slot[i].posX + (world.player.slot[i].l - world.player.slot[i].h * world.player.slot[i].p) / 2 + 2, world.player.slot[i].posY + world.player.slot[i].h - (world.player.slot[i].h - world.player.slot[i].h * world.player.slot[i].p) / 2 - 4);
          } else {
            image(objet[world.player.slot[i].o].texture, world.player.slot[i].posX + world.player.slot[i].l / 2 - (world.player.slot[i].l * world.player.slot[i].p) / 2, world.player.slot[i].posY + world.player.slot[i].h / 2 - (world.player.slot[i].l * world.player.slot[i].p) / 2, world.player.slot[i].l * world.player.slot[i].p, world.player.slot[i].l * world.player.slot[i].p);
            text(world.player.slot[i].q, world.player.slot[i].posX + (world.player.slot[i].l - world.player.slot[i].l * world.player.slot[i].p) / 2 + 2, world.player.slot[i].posY + world.player.slot[i].h - (world.player.slot[i].h - world.player.slot[i].l * world.player.slot[i].p) / 2 - 4);
          }
        }
      }
    }
  
    for (var i = 0; i < world.player.slot.length; i++) {
      if (i < tailleInventaire[typeInventaire]) {
        if (((sourisX != selectX || sourisY != selectY) && world.player.slot[i].click == true) && objet[world.player.slot[i].o].texture != undefined) { // slot en déplacement
          if (world.player.slot[i].l >= world.player.slot[i].h) {
            image(objet[world.player.slot[i].o].texture, sourisX - (world.player.slot[i].h * world.player.slot[i].p) / 2, sourisY - (world.player.slot[i].h * world.player.slot[i].p) / 2, world.player.slot[i].h * world.player.slot[i].p, world.player.slot[i].h * world.player.slot[i].p);
            text(world.player.slot[i].q, sourisX - (world.player.slot[i].h * world.player.slot[i].p) / 2 + 2, sourisY - (world.player.slot[i].h * world.player.slot[i].p) / 2 + world.player.slot[i].h * world.player.slot[i].p - 5);
          } else {
            image(objet[world.player.slot[i].o].texture, sourisX - (world.player.slot[i].l * world.player.slot[i].p) / 2, sourisY - (world.player.slot[i].l * world.player.slot[i].p) / 2, world.player.slot[i].l * world.player.slot[i].p, world.player.slot[i].l * world.player.slot[i].p);
            text(world.player.slot[i].q, sourisX - (world.player.slot[i].l * world.player.slot[i].p) / 2 + 2, sourisY - (world.player.slot[i].l * world.player.slot[i].p) / 2 + world.player.slot[i].l * world.player.slot[i].p - 5);
          }
        }
      }
    }
  }