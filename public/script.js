/*
Fait par Brochard Julien et Loussouarn Kylian 165L / 282I
Avec les textures de www.kenney.nl
*/

function setup(){
    


  

  textFont(pixelFont, "15px", "");
  noStroke();

  
  //createNewWorld();

  // démarre le jeux sur le menu accueil
  menu.accueil();

} // fin setup
/* ----- FIN BIOMES ----- */

function createNewWorld() {
  localWorld.push(
    {
      player: {
        posX: 0,
        posY: 0,
        taille: 30,
        speed: 4,
        jump: 12,
        deplacement: 5,
        //nombre de saut ou "infini" ou "vol" ou "non"
        noclip: false,
        accelerationX: 0,
        accelerationY: 0,
        range: 150,
        //nombre de portee en pixels ou "infini"
        facing: "est",
        currentSlot: "none",
        slot: []
      },
      camera: {
        posX: 0,
        posY: 0,
        deplacement : "pousse",
        // "centre" pour rencentrer la camera quand on s'approche du bord ou "pousse" pour bouger la camera dans la direction ou on avance ou "continu" pour centrer constament
        // ou "none" pour aucun deplacement auto de la camera
        distanceX : 10*cube,
        distanceY : 5*cube,
        pousseX : 10,
        pousseY : 5
      },
      dim: [
        
      ]
    }
  );
  
  currentWorld = localWorld.length-1;

  createSlot(localWorld[currentWorld], 10, gameY + (pageY - gameY - 50) / 2, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 1, "PiB");
  createSlot(localWorld[currentWorld], 70, gameY + (pageY - gameY - 50) / 2, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 1, "HaB");
  createSlot(localWorld[currentWorld], 130, gameY + (pageY - gameY - 50) / 2, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 1, "PeB");
  createSlot(localWorld[currentWorld], 190, gameY + (pageY - gameY - 50) / 2, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 250, gameY + (pageY - gameY - 50) / 2, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 310, gameY + (pageY - gameY - 50) / 2, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 370, gameY + (pageY - gameY - 50) / 2, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 430, gameY + (pageY - gameY - 50) / 2, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 4, "TdC");

  createSlot(localWorld[currentWorld], 10, gameY - 70 * 3 + 135, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 64, "B");
  createSlot(localWorld[currentWorld], 70, gameY - 70 * 3 + 135, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 64, "p");
  createSlot(localWorld[currentWorld], 130, gameY - 70 * 3 + 135, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 64, "P");
  createSlot(localWorld[currentWorld], 190, gameY - 70 * 3 + 135, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 64, "LF");
  createSlot(localWorld[currentWorld], 250, gameY - 70 * 3 + 135, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 310, gameY - 70 * 3 + 135, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 370, gameY - 70 * 3 + 135, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 430, gameY - 70 * 3 + 135, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");

  // Slot poubelle
  createSlot(localWorld[currentWorld], 490, gameY - 70 * 3 + 135, 50, 50, 0.75, texture.inventaire.poubelle, texture.inventaire.poubelle2, 0, " ", false, false, true);

  createSlot(localWorld[currentWorld], 10, gameY - 70 * 3 + 75, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 70, gameY - 70 * 3 + 75, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 130, gameY - 70 * 3 + 75, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 190, gameY - 70 * 3 + 75, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 250, gameY - 70 * 3 + 75, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 310, gameY - 70 * 3 + 75, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 370, gameY - 70 * 3 + 75, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 430, gameY - 70 * 3 + 75, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");

  createSlot(localWorld[currentWorld], 10, gameY - 70 * 3 + 20, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 70, gameY - 70 * 3 + 20, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 130, gameY - 70 * 3 + 20, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 190, gameY - 70 * 3 + 20, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 250, gameY - 70 * 3 + 20, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 310, gameY - 70 * 3 + 20, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 370, gameY - 70 * 3 + 20, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");
  createSlot(localWorld[currentWorld], 430, gameY - 70 * 3 + 20, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ");

  // sortie craft
  createSlot(localWorld[currentWorld], 730, gameY - 70 * 3 + 75, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ", false, true);

  // 9 cases de craft
  //4 premiers slots petite table de craft
  createSlot(localWorld[currentWorld], 550, gameY - 70 * 3 + 20, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ", false, true);
  createSlot(localWorld[currentWorld], 610, gameY - 70 * 3 + 20, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ", false, true);
  createSlot(localWorld[currentWorld], 550, gameY - 70 * 3 + 75, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ", false, true);
  createSlot(localWorld[currentWorld], 610, gameY - 70 * 3 + 75, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ", false, true);

  // 5 autres slots grosse table de craft
  createSlot(localWorld[currentWorld], 670, gameY - 70 * 3 + 20, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ", false, true);
  createSlot(localWorld[currentWorld], 670, gameY - 70 * 3 + 75, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ", false, true);
  createSlot(localWorld[currentWorld], 550, gameY - 70 * 3 + 135, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ", false, true);
  createSlot(localWorld[currentWorld], 610, gameY - 70 * 3 + 135, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ", false, true);
  createSlot(localWorld[currentWorld], 670, gameY - 70 * 3 + 135, 50, 50, 0.75, texture.inventaire.tan, texture.inventaire.brown, 0, " ", false, true);

  createNewDim();

  // DOIT ÊTRE APRES LA CREATION DES SLOTS
  tailleInventaire = [8,38,localWorld[currentWorld].player.slot.length];


  // chargements de choses de bases pour le fonctionnement du jeux


}

function createNewDim() {
    localWorld[currentWorld].dim.push(
      {
        map: [],
        deco: [],
        dur: [],

        mapX: 0,
        mapY: 0,
        biomeL: undefined, // longueur du biome en cours du côté est
        nbiomeL: undefined, // longueur du biome en cours du côté ouest
        biomeS: undefined, // Biome suivant du côté est
        nbiomeS: undefined, // Biome suivant du côté ouest
        T: [], // Hauteur du terrain
        B: [], // Biomes
        quantite: 0, // quantite de bloc que l'on monte ou descend à la fois du côté est
        nquantite: 0, // quantite de bloc que l'on monte ou descend à la fois du côté ouest
        sable: [],
        project: []
      }
    );

  currentDim = localWorld[currentWorld].dim.length-1;

  localWorld[currentWorld].dim[currentDim].T = [30];
  localWorld[currentWorld].dim[currentDim].B = [Math.floor(Math.random()*(biome.length))];
  localWorld[currentWorld].dim[currentDim].T["nlength"] = 1;
  localWorld[currentWorld].dim[currentDim].B["nlength"] = 1;
  localWorld[currentWorld].dim[currentDim].biomeL = Math.floor(Math.random()*(biome[localWorld[currentWorld].dim[currentDim].B[0]].longRdm)) + biome[localWorld[currentWorld].dim[currentDim].B[0]].longMin; // longueur du biome en cours du côté est
  localWorld[currentWorld].dim[currentDim].nbiomeL = Math.floor(Math.random()*(biome[localWorld[currentWorld].dim[currentDim].B[0]].longRdm)) + biome[localWorld[currentWorld].dim[currentDim].B[0]].longMin; // longueur du biome en cours du côté ouest
  localWorld[currentWorld].dim[currentDim].biomeS = Math.floor(Math.random()*(biome.length)); // Biome suivant du côté est
  localWorld[currentWorld].dim[currentDim].nbiomeS = Math.floor(Math.random()*(biome.length)); // Biome suivant du côté ouest

  // Calcul de la hauteur de la map
  localWorld[currentWorld].dim[currentDim].mapY = ciel + sol;
  
  // Création de tableaux de la taille de la map
  for (var y = 0; y < localWorld[currentWorld].dim[currentDim].mapY; y++) {
    localWorld[currentWorld].dim[currentDim].map.push([]);
    localWorld[currentWorld].dim[currentDim].deco.push([]);
    localWorld[currentWorld].dim[currentDim].dur.push([]);
  }

  initialisation(localWorld[currentWorld], 500);
}

/* +++++ REPETITION CONSTANTE A 30FPS +++++ */
function draw() {
  tick++;
  switch(menu.actuel){
    case "jeux":
      textFont(pixelFont, "15px", "");
      deplacement(localWorld[currentWorld]);
      actionSouris(localWorld[currentWorld]);
      showMap(localWorld[currentWorld]);
      action(localWorld[currentWorld]);
      showSlots(localWorld[currentWorld]);
      showButtons();
      if(info == true){
        showInfo(localWorld[currentWorld]);
      }
      break;
    case "drop":
      image(texture.screenshot,0, 0, pageY*2.526, pageY);
      image(texture.inventaire.tan,pageX/2 -75, pageY/2 -75, 150, 150);
      image(texture.inventaire.import, pageX/2 -75, pageY/2 -75, 150, 150);
      showButtons();
      break;
    case "accueil":
      textFont(pixelFont, "100px", "");
      image(texture.screenshot,0, 0, pageY*2.526, pageY);
      fill("black");
      text("Terrascript", pageX/2-200, 100);
      textFont(pixelFont, "50px", "");
      showButtons();
      break;
    case "mondes":
      textFont(pixelFont, "100px", "");
      image(texture.screenshot,0, 0, pageY*2.526, pageY);
      fill("black");
      text("Mondes", pageX/2-200, 100);
      textFont(pixelFont, "50px", "");
      showButtons();
      break;
    case "contrôles":
      textFont(pixelFont, "50px", "");
      fill("silver");
      rect(0, 0, pageX, pageY);
      fill("black");
      text("Contrôles", 100, 50);
      image(texture.touchesClavier, 100, 100, pageX-200, pageY-150);
      showButtons();
      break;
    case "crédits":
      textFont(pixelFont, "50px", "");
      fill("silver");
      rect(0, 0, pageX, pageY);
      fill("black");
      text("Crédits", 100,100);
      textFont(pixelFont, "20px", "");
      text("Textures par:", 100,150);
      text("Kenney, accessibles sur www.kenney.nl", 100,170);
      text("Programmé par:", 100,250);
      text("Brochard Julien", 100,275);
      text("Loussouarn Kylian", 100,300);
      showButtons();
      break;
    case "crafts":
      textFont(pixelFont, "16px", "");
      fill("silver");
      rect(0, 0, pageX, pageY);
      fill("black");
      text("Jouez dans un monde infini généré aléatoirement, explorez le monde, récoltez des ressources pour pouvoir fabriquer de l’équipement et enfin, laissez part à votre imagination pour construire ce que vous voulez !", 10,20);
      afficherCraft(0, 10 + 175*0, 30, 30);
      afficherCraft(1, 10 + 175*1, 30, 30);
      afficherCraft(2, 10 + 175*2, 30, 30);
      afficherCraft(3, 10 + 175*3, 30, 30);
      afficherCraft(4, 10 + 175*4, 30, 30);
      afficherCraft(5, 10 + 175*0, 30 + 135*1, 30);
      afficherCraft(6, 10 + 175*0, 30 + 135*2, 30);
      afficherCraft(7, 10 + 175*0, 30 + 135*3, 30);
      afficherCraft(8, 10 + 175*1, 30 + 135*1, 30);
      afficherCraft(9, 10 + 175*1, 30 + 135*2, 30);
      afficherCraft(10, 10 + 175*1, 30 + 135*3, 30);
      afficherCraft(11, 10 + 175*2, 30 + 135*1, 30);
      afficherCraft(12, 10 + 175*2, 30 + 135*2, 30);
      afficherCraft(13, 10 + 175*2, 30 + 135*3, 30);
      afficherCraft(14, 10 + 175*3, 30 + 135*1, 30);
      afficherCraft(15, 10 + 175*3, 30 + 135*2, 30);
      afficherCraft(16, 10 + 175*3, 30 + 135*3, 30);
      afficherCraft(17, 10 + 175*4, 30 + 135*1, 30);
      afficherCraft(18, 10 + 175*4, 30 + 135*2, 30);
      afficherCraft(19, 10 + 175*4, 30 + 135*3, 30);
      showButtons();
      break;
    case "cheats":
      textFont(pixelFont, "25px", "");
      fill("silver");
      rect(0, 0, pageX, pageY);
      showButtons();
      break;
  }
  document.title = "Terrascript: " + menu.actuel;
}
/* ----- FIN REPETITION ----- */



/* +++++ MENUS: CHANGEMENTS DE MENUS +++++ */
var menu = {
  actuel: "accueil",
  accueil: function(){
    this.actuel = "accueil";
    bouton = [];
    boutons(pageX/2-125, gameY/ 2-50, 250, 50, "menu.mondes();", texture.inventaire.tan, texture.inventaire.brown, "Jouer", 70, 40);
    boutons(pageX/2-125, gameY/ 2+50, 250, 50, "menu.controles();", texture.inventaire.tan, texture.inventaire.brown, "Contrôles", 35, 40);
    boutons(pageX/2-125, gameY/ 2+150, 250, 50, "menu.credits();", texture.inventaire.tan, texture.inventaire.brown, "Crédits", 55, 40);
  },
  mondes: function(){
    this.actuel = "mondes";
    bouton = [];
    boutons(100, 100, 400, 50, "createNewWorld();menu.mondes();", texture.inventaire.tan, texture.inventaire.brown, "Créer Monde", 70, 40);
    for(let i = 0; i < localWorld.length; i++){
      boutons(125, 200 + 75*i, 250, 50, "currentWorld="+ i + ";menu.jeux();", texture.inventaire.tan, texture.inventaire.brown, "Monde " + (i+1), 40, 40);
      boutons(390, 200 + 75*i, 50, 50, "currentWorld="+ i +";downloadCurrentWorld()", texture.inventaire.tan, texture.inventaire.brown, texture.inventaire.save, 0.75);
    }
    boutons(pageX-60, gameY + (pageY - gameY - 50) / 2, 50, 50, "menu.accueil();", texture.inventaire.tan, texture.inventaire.brown, texture.inventaire.home, 0.75);
  },
  jeux: function(){
    this.actuel = "jeux";
    bouton = [];
    boutons(pageX-320, gameY + (pageY - gameY - 50) / 2, 50, 50, "downloadCurrentWorld()", texture.inventaire.tan, texture.inventaire.brown, texture.inventaire.save, 0.75);
    boutons(pageX-255, gameY + (pageY - gameY - 50) / 2, 50, 50, "menu.cheats();", texture.inventaire.tan, texture.inventaire.brown, texture.inventaire.wrench, 0.75);
    boutons(pageX-190, gameY + (pageY - gameY - 50) / 2, 50, 50, "menu.crafts();", texture.inventaire.tan, texture.inventaire.brown, texture.inventaire.question, 0.75);
    boutons(pageX-125, gameY + (pageY - gameY - 50) / 2, 50, 50, "if(jouerson == true){jouerson = false;//son.musique.pause();for(var i=0;i<bouton.length;i++){if(bouton[i].i==texture.inventaire.audioon){bouton[i].i=texture.inventaire.audiooff;}}}else{jouerson = true;//son.musique.play();for(var i=0;i<bouton.length;i++){if(bouton[i].i==texture.inventaire.audiooff){bouton[i].i=texture.inventaire.audioon;}}}", texture.inventaire.tan, texture.inventaire.brown, texture.inventaire.audioon, 0.75);
    boutons(pageX-60, gameY + (pageY - gameY - 50) / 2, 50, 50, "menu.accueil();", texture.inventaire.tan, texture.inventaire.brown, texture.inventaire.home, 0.75);
    if(typeInventaire > 0){
      boutons(730, gameY - 70 * 3 + 135, 50, 50, "crafting(localWorld[currentWorld]);", texture.inventaire.tan, texture.inventaire.brown, objet["TdC"].texture, 0.75);
    }
  },
  drop: function(){
    this.actuel = "drop";
    bouton = [];
    boutons(pageX-60, gameY + (pageY - gameY - 50) / 2, 50, 50, "menu.accueil();", texture.inventaire.tan, texture.inventaire.brown, texture.inventaire.home, 0.75);
  },
  controles: function(){
    this.actuel = "contrôles";
    bouton = [];
    boutons(pageX-60, gameY + (pageY - gameY - 50) / 2, 50, 50, "menu.accueil();", texture.inventaire.tan, texture.inventaire.brown, texture.inventaire.home, 0.75);
  },
  credits: function(){
    this.actuel = "crédits";
    bouton = [];
    boutons(pageX-60, gameY + (pageY - gameY - 50) / 2, 50, 50, "menu.accueil();", texture.inventaire.tan, texture.inventaire.brown, texture.inventaire.home, 0.75);
  },
  crafts: function(){
    this.actuel = "crafts";
    bouton = [];
    boutons(pageX-255, gameY + (pageY - gameY - 50) / 2, 50, 50, "menu.cheats();", texture.inventaire.tan, texture.inventaire.brown, texture.inventaire.wrench, 0.75);
    boutons(pageX-190, gameY + (pageY - gameY - 50) / 2, 50, 50, "menu.jeux();", texture.inventaire.tan, texture.inventaire.brown, texture.inventaire.gamepad, 0.75);
    boutons(pageX-125, gameY + (pageY - gameY - 50) / 2, 50, 50, "if(jouerson == true){jouerson = false;//son.musique.pause();for(var i=0;i<bouton.length;i++){if(bouton[i].i==texture.inventaire.audioon){bouton[i].i=texture.inventaire.audiooff;}}}else{jouerson = true;//son.musique.play();for(var i=0;i<bouton.length;i++){if(bouton[i].i==texture.inventaire.audiooff){bouton[i].i=texture.inventaire.audioon;}}}", texture.inventaire.tan, texture.inventaire.brown, texture.inventaire.audioon, 0.75);
    boutons(pageX-60, gameY + (pageY - gameY - 50) / 2, 50, 50, "menu.accueil();", texture.inventaire.tan, texture.inventaire.brown, texture.inventaire.home, 0.75);
  },
  cheats: function(){
    this.actuel = "cheats";
    bouton = [];
    boutons(pageX-255, gameY + (pageY - gameY - 50) / 2, 50, 50, "menu.jeux();", texture.inventaire.tan, texture.inventaire.brown, texture.inventaire.gamepad, 0.75);
    boutons(pageX-190, gameY + (pageY - gameY - 50) / 2, 50, 50, "menu.crafts();", texture.inventaire.tan, texture.inventaire.brown, texture.inventaire.question, 0.75);
    boutons(pageX-125, gameY + (pageY - gameY - 50) / 2, 50, 50, "if(jouerson == true){jouerson = false;//son.musique.pause();for(var i=0;i<bouton.length;i++){if(bouton[i].i==texture.inventaire.audioon){bouton[i].i=texture.inventaire.audiooff;}}}else{jouerson = true;//son.musique.play();for(var i=0;i<bouton.length;i++){if(bouton[i].i==texture.inventaire.audiooff){bouton[i].i=texture.inventaire.audioon;}}}", texture.inventaire.tan, texture.inventaire.brown, texture.inventaire.audioon, 0.75);
    boutons(pageX-60, gameY + (pageY - gameY - 50) / 2, 50, 50, "menu.accueil();", texture.inventaire.tan, texture.inventaire.brown, texture.inventaire.home, 0.75);
    
    boutons(pageX/2-210, 10, 200, 40, "localWorld[currentWorld].player.deplacement=parseInt(prompt('Ecrire un nombre de double saut'));", texture.inventaire.tan, texture.inventaire.brown, "Double sauts", 40, 25);
    boutons(pageX/2+10, 10, 200, 40, "localWorld[currentWorld].player.deplacement='vol';", texture.inventaire.tan, texture.inventaire.brown, "Vol", 75, 25);
    boutons(pageX/2-210, 60, 200, 40, "localWorld[currentWorld].player.range=parseInt(prompt('Ecrire un nombre pour une distance (en px)'));", texture.inventaire.tan, texture.inventaire.brown, "Portée en px", 40, 25);
    boutons(pageX/2+10, 60, 200, 40, "localWorld[currentWorld].player.range='infini';", texture.inventaire.tan, texture.inventaire.brown, "Portée infini", 40, 25);
    boutons(pageX/2-100, 110, 200, 40, "localWorld[currentWorld].player.noclip = !localWorld[currentWorld].player.noclip;", texture.inventaire.tan, texture.inventaire.brown, "Noclip", 65, 25);
    boutons(pageX/2-100, 160, 200, 40, "localWorld[currentWorld].player.speed=parseInt(prompt('Ecrire un nombre pour une vitesse'));", texture.inventaire.tan, texture.inventaire.brown, "Vitesse", 60, 25);
    boutons(pageX/2-100, 210, 200, 40, "localWorld[currentWorld].player.jump=parseInt(prompt('Ecrire un nombre pour une hauteur de saut'));", texture.inventaire.tan, texture.inventaire.brown, "Saut", 75, 25);
    boutons(pageX/2-100, 260, 200, 40, "localWorld[currentWorld].player.taille=parseInt(prompt('Ecrire un nombre pour une taille du player'));", texture.inventaire.tan, texture.inventaire.brown, "Taille", 65, 25);
    boutons(pageX/2-100, 310, 200, 40, "localWorld[currentWorld].camera.deplacement=prompt('Ecrire pousse, centre, continu ou rien');", texture.inventaire.tan, texture.inventaire.brown, "Camera", 65, 25);
    boutons(pageX/2-100, 360, 200, 40, "donner(prompt('Id de l item'),parseInt(prompt('Quantité d items')));", texture.inventaire.tan, texture.inventaire.brown, "Donner items", 40, 25);
  }
};

/* ----- FIN MENUS ----- */



// deplacement du player et entitées
function deplacement(world) {
  try {
    // deplacement vertical
    // descendre
    if (world.player.accelerationY < 0 && world.player.deplacement != "vol") {
      try {
        for (var i = 0; i <= -world.player.accelerationY; i++) {
          if(world.player.noclip == true){
            world.player.posY++;
          }else{
            //Ecrire(Math.floor((player.posX) / cube) +  " - " + Math.floor((player.posX + player.taille) / cube));
            for(var u = Math.floor(world.player.posX / cube); u <= Math.floor((world.player.posX + world.player.taille) / cube); u++){
              if(world.dim[currentDim].map[Math.floor((world.player.posY + world.player.taille + 1) / cube)][u] != " "){
                break;
              }
              else if( u == Math.floor((world.player.posX + world.player.taille) / cube)){
                world.player.posY++;
              }
            }
          }
        }
      } catch (err) {}
    } else if (world.player.deplacement == "vol" && bas == true) {
      try {
        for (var i = 0; i <= world.player.speed; i++) {
          if(world.player.noclip == true){
            world.player.posY++;
          }else{
            //Ecrire(Math.floor((player.posX) / cube) +  " - " + Math.floor((player.posX + player.taille) / cube));
            for(var u = Math.floor(world.player.posX / cube); u <= Math.floor((world.player.posX + world.player.taille) / cube); u++){
              if(world.dim[currentDim].map[Math.floor((world.player.posY + world.player.taille + 1) / cube)][u] != " "){
                break;
              }
              else if( u == Math.floor((world.player.posX + world.player.taille) / cube)){
                world.player.posY++;
              }
            }
          }
        }
      } catch (err) {}
    }
    // monter
    if (world.player.accelerationY > 0 && world.player.deplacement != "vol") {
      try {
        for (var i = 0; i <= world.player.accelerationY; i++) {
          if(world.player.noclip == true){
            world.player.posY--;
          }else{
            //Ecrire(Math.floor((player.posX) / cube) +  " - " + Math.floor((player.posX + player.taille) / cube));
            for(var u = Math.floor((world.player.posX) / cube); u <= Math.floor((world.player.posX + world.player.taille) / cube); u++){
              if(world.dim[currentDim].map[Math.floor((world.player.posY - 1) / cube)][u] != " "){
                break;
              }
              else if( u == Math.floor((world.player.posX + world.player.taille) / cube)){
                world.player.posY--;
              }
            }
          }
        }
      } catch (err) {}
    } else if (world.player.deplacement == "vol" && haut == true) {
      try {
        for (var i = 0; i <= world.player.speed; i++) {
          if(world.player.noclip == true){
            world.player.posY--;
          }else{
            //Ecrire(Math.floor((player.posX) / cube) +  " - " + Math.floor((player.posX + player.taille) / cube));
            for(var u = Math.floor((world.player.posX) / cube); u <= Math.floor((world.player.posX + world.player.taille) / cube); u++){
              if(world.dim[currentDim].map[Math.floor((world.player.posY - 1) / cube)][u] != " "){
                break;
              }
              else if( u == Math.floor((world.player.posX + world.player.taille) / cube)){
                world.player.posY--;
              }
            }
          }
        }
      } catch (err) {}
    }

    if (gauche == true && Math.abs(world.player.accelerationX - world.player.speed) <= accelerationMax) {
      world.player.accelerationX -= world.player.speed;
    }
    if(droite == true && Math.abs(world.player.accelerationX + world.player.speed) <= accelerationMax) {
      world.player.accelerationX += world.player.speed;
    }

    // deplacement horizontal v2
    // gauche
    if (world.player.accelerationX < 0 && world.player.deplacement != "vol") {
      try {
        for (var i = 0; i <= -world.player.accelerationX; i++) {
          if(world.player.noclip == true){
            world.player.posX--;
          }else{
            //Ecrire(Math.floor((player.posX) / cube) +  " - " + Math.floor((player.posX + player.taille) / cube));
            for(var u = Math.floor(world.player.posY / cube); u <= Math.floor((world.player.posY + world.player.taille) / cube); u++){
              if(world.dim[currentDim].map[u][Math.floor((world.player.posX - 1) / cube)] != " "){
                break;
              }
              else if( u == Math.floor((world.player.posY + world.player.taille) / cube)){
                world.player.posX--;
              }
            }
          }
        }
      } catch (err) {}
    } else if (world.player.deplacement == "vol" && gauche == true) {
      try {
        for (var i = 0; i <= world.player.speed; i++) {
          if(world.player.noclip == true){
            world.player.posX--;
          }else{
            //Ecrire(Math.floor((player.posX) / cube) +  " - " + Math.floor((player.posX + player.taille) / cube));
            for(var u = Math.floor(world.player.posY / cube); u <= Math.floor((world.player.posY + world.player.taille) / cube); u++){
              if(world.dim[currentDim].map[u][Math.floor((world.player.posX - 1) / cube)] != " "){
                break;
              }
              else if( u == Math.floor((world.player.posY + world.player.taille) / cube)){
                world.player.posX--;
              }
            }
          }
        }
      } catch (err) {}
    }
    // droite
    if (world.player.accelerationX > 0 && world.player.deplacement != "vol") {
      try {
        for (var i = 0; i <= world.player.accelerationX; i++) {
          if(world.player.noclip == true){
            world.player.posX++;
          }else{
            //Ecrire(Math.floor((player.posX) / cube) +  " - " + Math.floor((player.posX + player.taille) / cube));
            for(var u = Math.floor((world.player.posY) / cube); u <= Math.floor((world.player.posY + world.player.taille) / cube); u++){
              if(world.dim[currentDim].map[u][Math.floor((world.player.posX + world.player.taille + 1) / cube)] != " "){
                break;
              }
              else if( u == Math.floor((world.player.posY + world.player.taille) / cube)){
                world.player.posX++;
              }
            }
          }
        }
      } catch (err) {}
    } else if (world.player.deplacement == "vol" && droite == true) {
      try {
        for (var i = 0; i <= world.player.speed; i++) {
          if(world.player.noclip == true){
            world.player.posX++;
          }else{
            //Ecrire(Math.floor((player.posX) / cube) +  " - " + Math.floor((player.posX + player.taille) / cube));
            for(var u = Math.floor((world.player.posY) / cube); u <= Math.floor((world.player.posY + world.player.taille) / cube); u++){
              if(world.dim[currentDim].map[u][Math.floor((world.player.posX + world.player.taille + 1) / cube)] != " "){
                break;
              }
              else if( u == Math.floor((world.player.posY + world.player.taille) / cube)){
                world.player.posX++;
              }
            }
          }
        }
      } catch (err) {}
    }

    // gravité player
    for(var u = Math.floor(world.player.posX / cube); u <= Math.floor((world.player.posX + world.player.taille) / cube); u++){
      if(world.dim[currentDim].map[Math.floor((world.player.posY + world.player.taille + 1) / cube)][u] != " "){
        world.player.accelerationX = 0;
        if(world.player.accelerationY < 0){
          world.player.accelerationY = 0;
          // son
          if(jouerson == true){
            if(world.dim[currentDim].map[Math.floor((world.player.posY + world.player.taille+1)/cube)][Math.floor((world.player.posX + world.player.taille/2)/cube)] != " "){
              switch(objet[world.dim[currentDim].map[Math.floor((world.player.posY + world.player.taille+1)/cube)][Math.floor((world.player.posX + world.player.taille/2)/cube)]].outil){
                case "pelle":
                  //son.player.tombeTerre.play();
                  break;
                case "pioche":
                   //son.player.tombePierre.play();
                   break;
                case "hache":
                   //son.player.tombeBois.play();
                   break;
              }
            }
          }
          // fin son
        }
        if(typeof world.player.deplacement == "number"){ // limite nombre de saut
          nombreSaut = world.player.deplacement;
        }
        break;
      }
      else if( u == Math.floor((world.player.posX + world.player.taille) / cube) && world.player.accelerationY >= vitesseChuteMax + gravite){
        world.player.accelerationY -= gravite;
      }
    }

    // gravité sable
    for (var i = 0; i < world.dim[currentDim].sable.length; i++) {
      world.dim[currentDim].sable[i].accelerationY -= gravite;
      world.dim[currentDim].sable[i].gravite(world, i);
    }
    // gravité projectile
    for (var i = 0; i < world.dim[currentDim].project.length; i++) {
      world.dim[currentDim].project[i].accelerationY -= gravite;
      world.dim[currentDim].project[i].gravite(world, i);
    }

    // deplacement auto camera
    switch(world.camera.deplacement){
      case "centre":
        if(world.player.posX - world.camera.posX * cube < world.camera.distanceX || world.player.posX - world.camera.posX * cube > pageX - world.camera.distanceX || world.player.posY - world.camera.posY * cube < world.camera.distanceY || world.player.posY - world.camera.posY * cube > gameY - world.camera.distanceY){
          centrerCamera(world);
        }
        break;
      case "pousse":
        if(world.player.posX - world.camera.posX * cube < world.camera.distanceX){
          world.camera.posX -= world.camera.pousseX;
        }
        if(world.player.posX - world.camera.posX * cube > pageX - world.camera.distanceX){
          world.camera.posX += world.camera.pousseX;
        }
        if(world.player.posY - world.camera.posY * cube < world.camera.distanceY){
          if(world.camera.posY-world.camera.pousseY >= 0){
            world.camera.posY -= world.camera.pousseY;
          }
          else{
            world.camera.posY = 0;
          }
        }
        if(world.player.posY - world.camera.posY * cube > gameY - world.camera.distanceY){
          if(world.camera.posY+world.camera.pousseY <= ciel+sol-gameY/cube){
            world.camera.posY += world.camera.pousseY;
          }
          else {
            world.camera.posY = ciel+sol-gameY/cube;
          }
        }
        break;
      case "continu":
        centrerCamera(world);
        break;
      default:
        break;
    }

  } catch (err) {}
}



function action(world){
    // ramasser items
    for(var i = 0; i< world.dim[currentDim].project.length; i++){
      if(world.dim[currentDim].project[i].b == false){
        if(world.dim[currentDim].project[i].posX >= world.player.posX && world.dim[currentDim].project[i].posX <= world.player.posX + world.player.taille && world.dim[currentDim].project[i].posY >= world.player.posY && world.dim[currentDim].project[i].posY <= world.player.posY + world.player.taille){
          donner(world, world.dim[currentDim].project[i].o, world.dim[currentDim].project[i].q);
          world.dim[currentDim].project.splice(i,1);
        }
      }
    }
}

let saveTest;

function save(i){
  saveTest = JSON.parse(JSON.stringify(localWorld[i]));
  //console.log(JSON.stringify(localWorld[i]));
}

function loadSave() {
  try{
    localWorld.push(JSON.parse(JSON.stringify(saveTest)));
    currentWorld = localWorld.length-1;
    //localWorld.push(prompt("entrez la sauvegarde"));
  } catch(err) {
    alert(err);
  }
}

function downloadCurrentWorld(){
  console.log("Téléchargement d'une sauvegarde ( world" + currentWorld + ".js")
  download("world" + (currentWorld+1) + ".js", JSON.stringify(localWorld[currentWorld]));
}

function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:application/x-javascript;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

// DRAG AND DROP FICHIER
var dropZone = document.getElementById('terrascript');

// Optional.   Show the copy icon when dragging over.  Seems to only work for chrome.
dropZone.addEventListener('dragover', function(e) {
    menu.drop();
    e.stopPropagation();
    e.preventDefault();
    e.dataTransfer.dropEffect = 'copy';
});

// Get file data on drop
dropZone.addEventListener('drop', function(e) {
    
    

    e.stopPropagation();
    e.preventDefault();
    
    var files = e.dataTransfer.files; // Array of all files

    for (var i=0, file; file=files[i]; i++) {
        if (file.type.match('image.*')) {
            var reader = new FileReader();

            reader.onload = function(e2) {
                console.log("Importation d'une image (pourquoi t'as fais sa ???)");
                // finished reading file data.
                var img = document.createElement('img');
                img.style.position= "absolute";
                img.style.top= 0;
                img.src= e2.target.result;
                document.body.appendChild(img);
            }

            reader.readAsDataURL(file); // start reading the file data.
        }
        else if(files[0].type==='application/x-javascript'){
          console.log("Importation d'une sauvegarde (" + files[i].name + ")");
          var reader = new FileReader();

            reader.onload = function(e2) {
                localWorld.push(JSON.parse(e2.target.result));
                menu.mondes(); // va dans le menu monde / reload les boutons monde
            }

          reader.readAsText(file); // start reading the file data.
        }
    }
});
// FIN DRAG AND DROP FICHIER

/* +++++ CASSER ET POSER DES BLOCS +++++ */
function actionSouris(world) {
  if (Math.sqrt(Math.pow(Math.abs(sourisX - (world.player.posX + Math.floor(world.player.taille / 2) - world.camera.posX * cube)), 2) + Math.pow(Math.abs(sourisY - (world.player.posY + Math.floor(world.player.taille / 2) - world.camera.posY * cube)), 2)) < world.player.range || world.player.range == "infini") {
    if (sourisgauche == true) {
      // Casser bloc de la map
      if (world.dim[currentDim].map[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] != " ") {
        world.dim[currentDim].dur[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)]--;
        if (world.player.currentSlot != "none" && objet[world.player.slot[world.player.currentSlot].o].type == "outil" && objet[world.player.slot[world.player.currentSlot].o].outil == objet[world.dim[currentDim].map[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)]].outil) {
          world.dim[currentDim].dur[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] -= objet[world.player.slot[world.player.currentSlot].o].mine;
        } else {
          world.dim[currentDim].dur[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] -= 1;
        }
        if (world.dim[currentDim].dur[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] <= 0) {
          if(objet[world.dim[currentDim].map[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)]].loot != " "){
            createProjectile(world.dim[currentDim], sourisX + world.camera.posX*cube, sourisY + world.camera.posY*cube, 0, 0, objet[world.dim[currentDim].map[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)]].loot, 1);
            //donner(objet[map[Math.floor(sourisY / cube + camera.posY)][Math.floor(sourisX / cube + camera.posX)]].loot, 1);
          }
          world.dim[currentDim].map[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] = " ";
          if (world.dim[currentDim].deco[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] != " ") {
            world.dim[currentDim].dur[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] = objet[world.dim[currentDim].deco[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)]].dur;
          } else {
            world.dim[currentDim].dur[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] = 0;
          }
          // son
          if(jouerson == true){
            //son.player.punch.play();
          }
          // fin son
        }
      }
      // Casser bloc de deco
      else if (world.dim[currentDim].deco[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] != " ") {
        world.dim[currentDim].dur[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)]--;
        if (world.player.currentSlot != "none" && objet[world.player.slot[world.player.currentSlot].o].type == "outil" && objet[world.player.slot[world.player.currentSlot].o].outil == objet[world.dim[currentDim].deco[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)]].outil) {
          world.dim[currentDim].dur[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] -= objet[world.player.slot[world.player.currentSlot].o].mine;
        } else {
          world.dim[currentDim].dur[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] -= 1;
        }
        if (world.dim[currentDim].dur[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] <= 0) {
          if(objet[world.dim[currentDim].deco[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)]].loot != " "){
            createProjectile(world.dim[currentDim], sourisX + world.camera.posX*cube, sourisY + world.camera.posY*cube, 0, 0, objet[world.dim[currentDim].deco[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)]].loot, 1);
            //donner(objet[deco[Math.floor(sourisY / cube + camera.posY)][Math.floor(sourisX / cube + camera.posX)]].loot, 1);
          }
          world.dim[currentDim].deco[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] = " ";
          world.dim[currentDim].dur[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] = 0;
          // son
          if(jouerson == true){
            //son.player.punch.play();
          }
          // fin son
        }
      }

    } else if (sourisdroite == true) {
      if (ctrl == false) {
        // Poser bloc sur la map
        if (world.dim[currentDim].map[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] == " ") {
          if (world.player.currentSlot != "none" && world.player.slot[world.player.currentSlot].q > 0 && objet[world.player.slot[world.player.currentSlot].o].type == "bloc") {
            world.dim[currentDim].map[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] = world.player.slot[world.player.currentSlot].o;
            world.dim[currentDim].dur[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] = objet[world.dim[currentDim].map[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)]].dur;
            world.player.slot[world.player.currentSlot].q--;
            if (world.player.slot[world.player.currentSlot].q < 1) {
              world.player.slot[world.player.currentSlot].o = " ";
              world.player.slot[world.player.currentSlot].q = 0;
            }
          } else if (world.dim[currentDim].deco[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] != " ") {
            objet[world.dim[currentDim].deco[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)]].click();
          }
        } else { // si on click sur un bloc map
          objet[world.dim[currentDim].map[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)]].click();
        }
      } else {
        // Poser bloc de deco
        if (world.dim[currentDim].deco[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] == " ") {
          if (world.player.currentSlot != "none" && world.player.slot[world.player.currentSlot].q > 0 && objet[world.player.slot[world.player.currentSlot].o].type == "bloc") {
            world.dim[currentDim].deco[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] = world.player.slot[world.player.currentSlot].o;
            world.dim[currentDim].dur[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)] = objet[world.dim[currentDim].deco[Math.floor(sourisY / cube + world.camera.posY)][Math.floor(sourisX / cube + world.camera.posX)]].dur;
            world.player.slot[world.player.currentSlot].q--;
            if (world.player.slot[world.player.currentSlot].q < 1) {
              world.player.slot[world.player.currentSlot].o = " ";
              world.player.slot[world.player.currentSlot].q = 0;
            }
          }
        }
/*else{ // si on click sur un bloc deco avec ctrl
          objet[deco[Math.floor(sourisY / cube + camera.posY)][Math.floor(sourisX / cube + camera.posX)]].click();
        }*/
      }
    }
  }
}
/* ----- FIN CASSER ET POSER DES BLOCS ----- */

/* +++++ Centre le player sur l'écran +++++ */
function centrerCamera(world){
  world.camera.posX = Math.floor((world.player.posX - (pageX / 2 - cube)) / cube);
  if(Math.floor((world.player.posY - (gameY / 2 - cube)) / cube) >= 0 && Math.floor((world.player.posY - (gameY / 2 - cube)) / cube) <= ciel+sol-gameY/cube){
    world.camera.posY = Math.floor((world.player.posY - (gameY / 2 - cube)) / cube);
  }
  else if(Math.floor((world.player.posY - (gameY / 2 - cube)) / cube) < 0){
    world.camera.posY = 0;
  }
  else if(Math.floor((world.player.posY - (gameY / 2 - cube)) / cube) > ciel+sol-gameY/cube){
    world.camera.posY = ciel+sol-gameY/cube;
  }
}
/* ----- FIN Centre le player sur l'écran ----- */

/* +++++ DONNE ITEMS AU player +++++ */
function donner (world, o, q){
  for (var i = 0; i < world.player.slot.length; i++) { // cherche un slot avec le même objet
    if (world.player.slot[i].o == o && world.player.slot[i].q != objet[world.player.slot[i].o].stack) {
      if(world.player.slot[i].q + q <= objet[world.player.slot[i].o].stack){
        world.player.slot[i].q += q;
      }
      else{
        q -= objet[o].stack - world.player.slot[i].q;
        world.player.slot[i].q = objet[o].stack;
        donner(world, o, q);
      }
      break;
    }
    else if (i == world.player.slot.length - 1) { // sinon cherche un slot vide
      for (var i = 0; i < world.player.slot.length; i++) {
        if (world.player.slot[i].o == " " && world.player.slot[i].remplissage == true) {
          if(q <= objet[o].stack){
            world.player.slot[i].o = o;
            world.player.slot[i].q = q;
          }
          else{
            q -= objet[o].stack;
            world.player.slot[i].o = o;
            world.player.slot[i].q = objet[o].stack;
            donner(world, o, q);
          }
          break;
        } else if( i == world.player.slot.length-1){ // sinon si l'inventaire est plein
          if(world.player.facing == "ouest"){
            createProjectile(world.dim[currentDim], world.player.posX+player.taille+1, world.player.posY, 1, 0, o, q);
          }else if(player.facing == "est"){
            createProjectile(world.dim[currentDim], world.player.posX-1, world.player.posY, -1, 0, o, q);
          }
          //createProjectile(sourisX + camera.posX*cube, sourisY + camera.posY*cube, 0, 0, o, q);
        }
      }
      break;
    }
  }
}
/* ----- FIN DONNE ITEMS AU player ----- */

/* +++++ FAIRE TOMBER ITEM DU player +++++ */
function drop(world, s, q){
  if(q == "tout"){ // lacher un slot entier
    if(world.player.facing == "ouest"){
      createProjectile(world.dim[currentDim], world.player.posX-1, world.player.posY, -3, 0, world.player.slot[s].o, world.player.slot[s].q);
      world.player.slot[s].o = " ";
      world.player.slot[s].q = 0;
    }else if(player.facing == "est"){
      createProjectile(world.dim[currentDim], world.player.posX+player.taille+1, world.player.posY, 3, 0, world.player.slot[s].o, world.player.slot[s].q);
      world.player.slot[s].o = " ";
      world.player.slot[s].q = 0;
    }
  }
  else{ // lacher un certain nombe d'item
    if(world.player.facing == "ouest"){
      if(world.player.slot[s].q >= q){
        createProjectile(world.dim[currentDim], world.player.posX-1, world.player.posY, -3, 0, world.player.slot[s].o, q);
        if(world.player.slot[s].q > q){
          world.player.slot[s].q -= q;
        }else{
          world.player.slot[s].o = " ";
          world.player.slot[s].q = 0;
        }
      }
    }else if(world.player.facing == "est"){
      if(world.player.slot[s].q >= q){
        createProjectile(world.dim[currentDim], world.player.posX+world.player.taille+1, world.player.posY, 3, 0, world.player.slot[s].o, q);
        if(world.player.slot[s].q > q){
          world.player.slot[s].q -= q;
        }else{
          world.player.slot[s].o = " ";
          world.player.slot[s].q = 0;
        }
      }
    }
  }
}
/* ----- FIN FAIRE TOMBER ITEM DU player ----- */

/* +++++ VERIFIER ET EXECUTER CRAFT +++++ */
function crafting(world){
  // prototype craft
  if(typeInventaire > 0){
    for(var i = 0; i < craft.length; i++){
      scan:
      for(var y = 0; y <= 2; y++){
        for(var x = 0; x <= 2; x++){  
          // x+3*y compte de 0 à 8
          //Ecrire(slot[slotCraft[x+3*y]].o + " " + (x+3*y) + " " + slotCraft[x+3*y] +  " " + craft[i][y][x]);
          if(world.player.slot[slotCraft[x+3*y]].o != craft[i][y][x]){
            break scan; // arrête les boucles de scan si une case ne correspond pas
          }
          else if(y == 2 && x == 2){
            // donne le résultat du craft
            if(world.player.slot[slotCraft[slotCraft.length-1]].o == " " || (world.player.slot[slotCraft[slotCraft.length-1]].o == craft[i][1][3] && world.player.slot[slotCraft[slotCraft.length-1]].q + craft[i][1][4] <= objet[world.player.slot[slotCraft[slotCraft.length-1]].o].stack)){
              for(var z = 0; z < 8; z++){
                if(world.player.slot[slotCraft[z]].q > 1){
                  world.player.slot[slotCraft[z]].q--;
                }
                else{
                  world.player.slot[slotCraft[z]].o = " ";
                  world.player.slot[slotCraft[z]].q = 0;
                }
              }
              world.player.slot[slotCraft[slotCraft.length-1]].o = craft[i][1][3];
              world.player.slot[slotCraft[slotCraft.length-1]].q += craft[i][1][4];
            }
          }
        }
      }
    }
  }
}

function afficherCraft(id, posX, posY, size){
  for(var y = 0; y <= 2; y++){
    for(var x = 0; x <= 2; x++){
      image(texture.inventaire.tan, posX + x*(size+10), posY + y*(size+10), size, size);
      if(objet[craft[id][y][x]].texture != undefined){
        image(objet[craft[id][y][x]].texture, posX + x*(size+10)+(size*0.25)/2, posY + y*(size+10)+(size*0.25)/2, size*0.75, size*0.75);
      }
    }
    image(texture.inventaire.tan, posX + 3*(size+10), posY + 1*(size+10), size, size);
    if(objet[craft[id][1][3]].texture != undefined){
      image(objet[craft[id][1][3]].texture, posX + 3*(size+10)+(size*0.25)/2, posY + 1*(size+10)+(size*0.25)/2, size*0.75, size*0.75);
    }
  }
}
/* ----- FIN VERIFIER ET EXECUTER CRAFT ----- */

var PYmax = 10;
var PXmax = 10;
var Pdiviseur = 15;

// fonction créant un objet projectile (item)
function createProjectile(dim, posX, posY, aX, aY, o, q, b=false) {
  dim.project[dim.project.length] = {
    posX: posX,
    posY: posY,
    o: o,
    q: q,
    b : b,
    accelerationX: aX,
    accelerationY: aY,
    gravite: function(world, id) {
      try {
        //valeur maxY
        if (this.accelerationY > PYmax) {
          this.accelerationY = PYmax;
        }
        //valeur maxX
        if (this.accelerationX > PXmax) {
          this.accelerationX = PXmax;
        } else if (this.accelerationX < -PXmax) {
          this.accelerationX = -PXmax;
        }

        // Projection verticale
        if (this.accelerationY < 0) {
          if (this.posY >= (dim.mapY - 1) * cube) {
            dim.project.splice(id, 1);
          } else if (dim.map[Math.floor((this.posY + 1) / cube)][Math.floor(this.posX / cube)] == " ") {
            for (var i = 0; i < -this.accelerationY; i++) {
              if (dim.map[Math.floor((this.posY + 1) / cube)][Math.floor(this.posX / cube)] == " ") {
                this.posY++;
              } else if(b == true){ // collision sol -> pose bloc
                dim.map[Math.floor(this.posY / cube)][Math.floor(this.posX / cube)] = "P";
                dim.dur[Math.floor(this.posY / cube)][Math.floor(this.posX / cube)] = objet[dim.map[Math.floor(this.posY / cube)][Math.floor(this.posX / cube)]].dur;
                dim.project.splice(id, 1);
                break;
              }
              else{
                this.accelerationY = 0; // collision sol
                if(this.accelerationX > 0){
                  this.accelerationX--;
                }
                else if(this.accelerationX < 0){
                  this.accelerationX++;
                }
                break;
              }
            }
          } else if(b == true){ // collision sol -> pose bloc
            dim.map[Math.floor(this.posY / cube)][Math.floor(this.posX / cube)] = "P";
            dim.dur[Math.floor(this.posY / cube)][Math.floor(this.posX / cube)] = objet[dim.map[Math.floor(this.posY / cube)][Math.floor(this.posX / cube)]].dur;
            dim.project.splice(id, 1);
          }
          else{
            this.accelerationY = 0; // collision sol
            if(this.accelerationX > 0){
              this.accelerationX--;
            }
            else if(this.accelerationX < 0){
              this.accelerationX++;
            }
          }
        } else if (this.accelerationY > 0) {

          for (var i = 0; i <= this.accelerationY; i++) {
            if (dim.map[Math.floor((this.posY - 1) / cube)][Math.floor(this.posX / cube)] == " ") {
              this.posY--;
            } else {
              this.accelerationY = 0; // collision plafond
              break;
            }
          }
        }
        // Projection horizontale
        if (this.accelerationX < 0) {
          if (dim.map[Math.floor(this.posY / cube)][Math.floor((this.posX - 1) / cube)] == " ") {
            for (var i = 0; i < -this.accelerationX; i++) {
              if (dim.map[Math.floor(this.posY / cube)][Math.floor((this.posX - 1) / cube)] == " ") {
                this.posX--;
              } else {
                this.accelerationX = 0;
                //map[Math.floor(this.posY/cube)][Math.floor(this.posX/cube)] = "P";
                //project.splice(id, 1);
                break;
              }
            }
          } else {
            this.accelerationX = 0;
            //map[Math.floor(this.posY/cube)][Math.floor(this.posX/cube)] = "P";
            //project.splice(id, 1);
          }
        } else if (this.accelerationX > 0) {
          if (dim.map[Math.floor(this.posY / cube)][Math.floor((this.posX + 1) / cube)] == " ") {
            for (var i = 0; i <= this.accelerationX; i++) {
              if (dim.map[Math.floor((this.posY) / cube)][Math.floor((this.posX + 1) / cube)] == " ") {
                this.posX++;
              } else {
                this.accelerationX = 0;
                //map[Math.floor(this.posY/cube)][Math.floor(this.posX/cube)] = "P";
                //project.splice(id, 1);
                break;
              }
            }
          } else {
            this.accelerationX = 0;
            //map[Math.floor(this.posY/cube)][Math.floor(this.posX/cube)] = "P";
            //project.splice(id, 1);
          }
        }
      } catch (err) {}
    }
  };
}

// fonction créant un objet sable (bloc qui tombe)
function createFallingBlock(dim, posX, posY, o) {
  //alert("sable");
  dim.sable[dim.sable.length] = {
    posX: posX * cube,
    posY: posY * cube,
    o: o,
    accelerationY: 0,
    gravite: function(world, id) {
      try {
        if (this.posY >= (dim.mapY - 1) * cube) {
          dim.sable.splice(id, 1);
        } else if (dim.map[Math.floor(this.posY / cube) + 1][Math.floor(this.posX / cube)] == " ") {
          for (var i = 0; i < -this.accelerationY; i++) {
            if (dim.map[Math.floor(this.posY / cube) + 1][Math.floor(this.posX / cube)] == " ") {
              this.posY++;
            } else { // collision sol -> pose bloc
              dim.map[Math.floor(this.posY / cube)][Math.floor(this.posX / cube)] = this.o;
              dim.dur[Math.floor(this.posY / cube)][Math.floor(this.posX / cube)] = objet[dim.map[Math.floor(this.posY / cube)][Math.floor(this.posX / cube)]].dur;
              dim.sable.splice(id, 1);
              break;
            }
          }
        } else { // collision sol -> pose bloc
          dim.map[Math.floor(this.posY / cube)][Math.floor(this.posX / cube)] = this.o;
          dim.dur[Math.floor(this.posY / cube)][Math.floor(this.posX / cube)] = objet[dim.map[Math.floor(this.posY / cube)][Math.floor(this.posX / cube)]].dur;
          dim.sable.splice(id, 1);
        }
      } catch (err) {}
    }
  };
}




/* +++++ VARIABLES ENTREES CLAVIER SOURIS +++++ */
var droite = false;
var gauche = false;

var haut = false;
var bas = false;

var ctrl = false;
var shift = false;

var sourisgauche = false;
var sourisdroite = false;

var sourisX;
var sourisY;

var selectX;
var selectY;
/* ----- FIN VARIABLES ENTREES CLAVIER SOURIS ----- */

// touches enfoncées
document.onkeydown = function(event) {
  switch (event.keyCode) {

  case 81:
    // q
    
    gauche = true;
    localWorld[currentWorld].player.facing = "ouest";
    break;

  case 90: // z
  case 32: // espace
    try {
      if ((typeof localWorld[currentWorld].player.deplacement == "number" && nombreSaut > 0) || localWorld[currentWorld].player.deplacement == "infini" || (localWorld[currentWorld].player.deplacement != "non" && (localWorld[currentWorld].dim[currentDim].map[Math.floor((localWorld[currentWorld].player.posY + localWorld[currentWorld].player.taille + 1) / cube)][Math.floor((localWorld[currentWorld].player.posX + localWorld[currentWorld].player.taille) / cube)] != " " || localWorld[currentWorld].dim[currentDim].map[Math.floor((localWorld[currentWorld].player.posY + localWorld[currentWorld].player.taille + 1) / cube)][Math.floor(localWorld[currentWorld].player.posX / cube)] != " "))) {
        localWorld[currentWorld].player.accelerationY = localWorld[currentWorld].player.jump;
        if (typeof localWorld[currentWorld].player.deplacement == "number" && localWorld[currentWorld].player.deplacement > 0) {
          nombreSaut--;
        }
      }
    } catch (err) {}
    haut = true;
    break;

  case 68:
    // d
    droite = true;
    localWorld[currentWorld].player.facing = "est";
    break;

  case 83:
    bas = true;
    break;
  case 17:
    //ctrl
    ctrl = true;
    break;

  case 16:
    shift = true;
    break;

  case 37:
    // flèche gauche
    localWorld[currentWorld].camera.posX--;
    break;

  case 39:
    // flèche droite
    localWorld[currentWorld].camera.posX++
    break;

  case 38:
    // flèche haut
    if (localWorld[currentWorld].camera.posY > 0) {
      localWorld[currentWorld].camera.posY--;
    }
    break;

  case 40:
    // flèche bas
    if (localWorld[currentWorld].camera.posY < localWorld[currentWorld].dim[currentDim].mapY - gameY / cube) {
      localWorld[currentWorld].camera.posY++;
    }
    break;

  case 109:
    // -
    loadMap(world, 500, "ouest");
    break;

  case 107:
    //+
    loadMap(world, 500, "est");
    break;

  case 80:
    // P
    createProjectile(localWorld[currentWorld].dim[currentDim], Math.floor(localWorld[currentWorld].player.posX+localWorld[currentWorld].player.taille/2), localWorld[currentWorld].player.posY, (sourisX - (localWorld[currentWorld].player.posX - localWorld[currentWorld].camera.posX * cube)) / Pdiviseur, Math.abs(sourisY - (localWorld[currentWorld].player.posY - localWorld[currentWorld].camera.posY * cube)) / Pdiviseur, "Cha", 1, true);
    break;
  case 88:
    // X
    if(localWorld[currentWorld].player.currentSlot != "none" && localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].o != " "){
      if(ctrl == false){
        drop(localWorld[currentWorld], localWorld[currentWorld].player.currentSlot, 1);
      } else {
        drop(localWorld[currentWorld], localWorld[currentWorld].player.currentSlot, "tout");
      }
    }
    break;
  case 73:
    // I
    if(menu.actuel == "jeux"){
      if (typeInventaire > 0) {
        typeInventaire = 0;
        bouton.splice(bouton.length-1,1);
        for(var i = 0; i < localWorld[currentWorld].player.slot.length; i++){
          if(localWorld[currentWorld].player.slot[i].drop == true && localWorld[currentWorld].player.slot[i].o != " "){
            drop(localWorld[currentWorld], i,localWorld[currentWorld].player.slot[i].q);
            localWorld[currentWorld].player.slot[i].o = " ";
            localWorld[currentWorld].player.slot[i].q = 0;
          }
        }
        //son.player.fermeInventaire.play()
      } else {
        typeInventaire = 1;
        boutons(730, gameY - 70 * 3 + 135, 50, 50, "crafting(localWorld[currentWorld]);", texture.inventaire.tan, texture.inventaire.brown, objet["TdC"].texture, 0.75);
        //son.player.ouvreInventaire.play()
      }
    }
    break;
  case 27:
    // escape
    info = !info;
    break;
  case 222:
    // carré ²
    eval(prompt("code cheat"));
    break;

  case 49:
  case 97:
    if (localWorld[currentWorld].player.currentSlot != "none") {
      localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
    }
    localWorld[currentWorld].player.slot[0].select = true;
    localWorld[currentWorld].player.currentSlot = 0;
    break;
  case 50:
  case 98:
    if (localWorld[currentWorld].player.currentSlot != "none") {
      localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
    }
    localWorld[currentWorld].player.slot[1].select = true;
    localWorld[currentWorld].player.currentSlot = 1;
    break;
  case 51:
  case 99:
    if (localWorld[currentWorld].player.currentSlot != "none") {
      localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
    }
    localWorld[currentWorld].player.slot[2].select = true;
    localWorld[currentWorld].player.currentSlot = 2;
    break;
  case 52:
  case 100:
    if (localWorld[currentWorld].player.currentSlot != "none") {
      localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
    }
    localWorld[currentWorld].player.slot[3].select = true;
    localWorld[currentWorld].player.currentSlot = 3;
    break;
  case 53:
  case 101:
    if (localWorld[currentWorld].player.currentSlot != "none") {
      localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
    }
    localWorld[currentWorld].player.slot[4].select = true;
    localWorld[currentWorld].player.currentSlot = 4;
    break;
  case 54:
  case 102:
    if (localWorld[currentWorld].player.currentSlot != "none") {
      localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
    }
    localWorld[currentWorld].player.slot[5].select = true;
    localWorld[currentWorld].player.currentSlot = 5;
    break;
  case 55:
  case 103:
    if (localWorld[currentWorld].player.currentlot != "none") {
      localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
    }
    localWorld[currentWorld].player.slot[6].select = true;
    localWorld[currentWorld].player.currentSlot = 6;
    break;
  case 56:
  case 104:
    if (localWorld[currentWorld].player.currentSlot != "none") {
      localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
    }
    localWorld[currentWorld].player.slot[7].select = true;
    localWorld[currentWorld].player.currentSlot = 7;
    break;
  case 57:
  case 105:
    if (localWorld[currentWorld].player.currentSlot != "none") {
      localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
    }
    //slot[8].select = true;
    //player.slot = 8;
    break;
  case 106:
    centrerCamera(localWorld[currentWorld]);
    break;
  case 48:
  case 96:
    if (localWorld[currentWorld].player.currentSlot != "none") {
      localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
    }
    //slot[9].select = true;
    localWorld[currentWorld].player.currentSlot = "none";
    break;
  }
};

// touches lachées
window.onkeyup = function(key) {
  switch (key.keyCode) {
  case 81:
    // q
    gauche = false;
    break;
  case 68:
    // d
    droite = false;
    break;
  case 90:
    // z
    haut = false;
    break;
  case 83:
    // s
    bas = false;
    break;
  case 17:
    //ctrl
    ctrl = false;
    break;
  case 16:
    shift = false;
    break;
  }
};

function click(e) {
  if (e.button == 0) {
    sourisgauche = true;
    clickButton();
    if(menu.actuel == "jeux"){
      selection();
    }
  } else if (e.button == 2) {
    sourisdroite = true;
    if(menu.actuel == "jeux"){
      poseSelection();
    }
  }
}

function nonclick(e) {
  if (e.button == 0) {
    sourisgauche = false;
    if(menu.actuel == "jeux"){
      nonselection();
    }
  } else if (e.button == 2) {
    sourisdroite = false;
  }
}

function souris(e) {
  // 0 gauche, 1 molette, 2 droit
  try {
    sourisX = e.pageX;
    sourisY = e.pageY;
    if (sourisX != selectX || sourisY != selectY) {
      selectX = undefined;
      selectY = undefined;
    }
  } catch (err) {}

}

document.addEventListener('mousedown', click);
document.addEventListener('mouseup', nonclick);
document.addEventListener('mousemove', souris, false);
document.addEventListener('mouseenter', souris, false);

document.addEventListener('contextmenu', function(e) {
  e.preventDefault();
  nonclick(e);
});


var info = false;


// BOUTONS ---------------------------------------------------------

var bouton = [];
function boutons(positionX = Math.random()*100 *10, positionY = Math.random()*50 *10, largeur = 40, hauteur = 20, fonction = "alert('-VIDE-');", fond = "none", fond2 = "none", image = "none", param1 = 1, param2 = 1){
  bouton[bouton.length] = {
    posX : positionX,
    posY : positionY,
    l : largeur,
    h : hauteur,
    p1 : param1,
    p2 : param2,
    fonction : fonction,
    f : fond,
    f2 : fond2,
    i : image
  };
}



function clickButton(){
  for (var i=0; i < bouton.length; i++) {
  if (sourisX > bouton[i].posX && sourisX <= bouton[i].posX + bouton[i].l && sourisY > bouton[i].posY && sourisY <= bouton[i].posY + bouton[i].h) {
      eval(bouton[i].fonction);
      //son.clickBouton.play();
    }
  }
}
// Inventaire mis là temporairement -------------------------------------------------------------------------------------

// créer des objets slots

function createSlot(world, positionX = Math.random()*100 * 10, positionY = Math.random()*50 * 10, largeur = 40, hauteur = 20, pourcent = 1, fond = "none", fond2 = "none", quantite = 0, o = " ", remplissage = true, drop = false, poubelle = false) {
  world.player.slot[world.player.slot.length] = {
    posX: positionX,
    posY: positionY,
    l: largeur,
    h: hauteur,
    o: o,
    p: pourcent,
    f: fond,
    f2: fond2,
    click: false,
    q: quantite,
    select: false,
    remplissage: remplissage,
    drop: drop,
    poubelle: poubelle
  };
}



// sélection d'un item dans un slot
function selection() {
  for (var i = 0; i < localWorld[currentWorld].player.slot.length; i++) {
    if (sourisX > localWorld[currentWorld].player.slot[i].posX && sourisY > localWorld[currentWorld].player.slot[i].posY && sourisX <= localWorld[currentWorld].player.slot[i].l + localWorld[currentWorld].player.slot[i].posX && sourisY <= localWorld[currentWorld].player.slot[i].h + localWorld[currentWorld].player.slot[i].posY && (i < tailleInventaire[typeInventaire])) {
      localWorld[currentWorld].player.slot[i].click = true;
      //if(slot[i].poubelle == false){ empèchant de sélectionner la case poubelle mais empêche aussi de vider la poubelle donc enlevé
      if (localWorld[currentWorld].player.currentSlot != "none") {
        localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
      }
      localWorld[currentWorld].player.slot[i].select = true;
      localWorld[currentWorld].player.currentSlot = i;
      //}
      selectX = sourisX;
      selectY = sourisY;
    }
  }
}

// lacher un item dans un slot
function nonselection() {
  for (var i = 0; i < localWorld[currentWorld].player.slot.length; i++) {
    if (localWorld[currentWorld].player.currentSlot != "none" && sourisX > localWorld[currentWorld].player.slot[i].posX && sourisY > localWorld[currentWorld].player.slot[i].posY && sourisX <= localWorld[currentWorld].player.slot[i].l + localWorld[currentWorld].player.slot[i].posX && sourisY <= localWorld[currentWorld].player.slot[i].h + localWorld[currentWorld].player.slot[i].posY && (i < tailleInventaire[typeInventaire])) {
      var temp = [localWorld[currentWorld].player.slot[i].q, localWorld[currentWorld].player.slot[i].o];
      if (localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].click == true && i != localWorld[currentWorld].player.currentSlot && objet[localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].o].texture != undefined) {
        if (localWorld[currentWorld].player.slot[i].poubelle == true) {
          localWorld[currentWorld].player.slot[i].q = localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].q;
          localWorld[currentWorld].player.slot[i].o = localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].o;
          localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].q = 0;
          localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].o = " ";
        } else {
          if (localWorld[currentWorld].player.slot[i].o == localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].o) { // empiler mêmes items
            if (localWorld[currentWorld].player.slot[i].q + localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].q <= objet[localWorld[currentWorld].player.slot[i].o].stack) { // si sa fais moins d'un stack
            localWorld[currentWorld].player.slot[i].q += localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].q;
            localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].q = 0;
            localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].o = " ";
            } else { // si sa fait plus d'un stack
            localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].q = localWorld[currentWorld].player.slot[i].q + localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].q - objet[localWorld[currentWorld].player.slot[i].o].stack;
            localWorld[currentWorld].player.slot[i].q = objet[localWorld[currentWorld].player.slot[i].o].stack;
            }
          } else {
            localWorld[currentWorld].player.slot[i].q = localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].q;
            localWorld[currentWorld].player.slot[i].o = localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].o;
            localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].q = temp[0];
            localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].o = temp[1];
          }
        }
      }
      //if(slot[i].poubelle == false){ ne sélectionne pas la poubelle quand on dépose un item mais remit
      localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].select = false;
      localWorld[currentWorld].player. slot[i].select = true;
      localWorld[currentWorld].player.currentSlot = i;
      //}
    }
  }
  for (var i = 0; i < localWorld[currentWorld].player.slot.length; i++) {
    localWorld[currentWorld].player.slot[i].click = false;
  }
}

function poseSelection(){
  for (var i = 0; i < localWorld[currentWorld].player.slot.length; i++) {
    if (localWorld[currentWorld].player.currentSlot != "none" && sourisX > localWorld[currentWorld].player.slot[i].posX && sourisY > localWorld[currentWorld].player.slot[i].posY && sourisX <= localWorld[currentWorld].player.slot[i].l + localWorld[currentWorld].player.slot[i].posX && sourisY <= localWorld[currentWorld].player.slot[i].h + localWorld[currentWorld].player.slot[i].posY && (i < tailleInventaire[typeInventaire])) {
      if(localWorld[currentWorld].player.slot[i].o == " "){
        localWorld[currentWorld].player.slot[i].o = localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].o;
        localWorld[currentWorld].player.slot[i].q = 1;
        if(localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].q > 1){
          localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].q--;
        }else{
          localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].o = " ";
          localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].q = 0;
        }
      }
      else if(localWorld[currentWorld].player.slot[i].o == localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].o){
        localWorld[currentWorld].player.slot[i].q++;
        if(localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].q > 1){
          localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].q--;
        }else{
          localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].o = " ";
          localWorld[currentWorld].player.slot[localWorld[currentWorld].player.currentSlot].q = 0;
        }
      }
      break;
    }
  }
}

setup();
frameRate(30);