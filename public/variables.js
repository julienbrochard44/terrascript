
let cube = 20;
let tick = 0;
let repet = 20; //45



let gravite = 2;

let barreInventaire = 60;

let pageX = window.innerWidth;
let pageY = window.innerHeight;
let gameY = Math.floor((pageY - barreInventaire) / cube) * cube;


let vitesseChuteMax = -20;
let accelerationMax = 8;

/* +++++ TEXTURES +++++ */
let texture = {
      player : {
        "BD":loadImage("textures/Custom/platformChar_idleDG-1.png"),
        "BG":loadImage("textures/Custom/platformChar_idleGG-1.png"),
        "M1D":loadImage("textures/Custom/platformChar_walk1DG.png"),
        "M1G":loadImage("textures/Custom/platformChar_walk1GG.png"),
        "M2D":loadImage("textures/Custom/platformChar_walk2DG.png"),
        "M2G":loadImage("textures/Custom/platformChar_walk2GG.png"),
        "SD":loadImage("textures/Custom/platformChar_jumpDG.png"),
        "SG":loadImage("textures/Custom/platformChar_jumpGG.png"),
        "GD":loadImage("textures/Custom/platformChar_duckGD.png"),
        "GG":loadImage("textures/Custom/platformChar_duckGG.png")
      },
      selecteur : {
        "blanc":loadImage("textures/Tiles/kenney_simplifiedplatformer/PNG/Tiles/platformPack_tile042.png"),
        "blancT":loadImage("textures/Custom/platformPack_tile042%20transparent.png"),
        "vert":loadImage("textures/Custom/platformPack_tile036%20sans%20rond.png"),
        "vertT":loadImage("textures/Tiles/kenney_simplifiedplatformer/PNG/Tiles/platformPack_tile021.png"),
        "invalide":loadImage("textures/Tiles/kenney_simplifiedplatformer/PNG/Tiles/platformPack_tile030.png")
      },
      inventaire : {
        "barre":loadImage("textures/UI/uipack-rpg/PNG/buttonLong_blue_pressed.png"),
        "craft":loadImage("textures/UI/uipack-rpg/PNG/panelInset_blue.png"),
        "tan":loadImage("textures/UI/pixeluipack/9-Slice/Ancient/tan.png"),
        "brown":loadImage("textures/UI/pixeluipack/9-Slice/Ancient/brown.png"),
        "poubelle":loadImage("textures/UI/gameicons/PNG/Black/2x/trashcan.png"),
        "poubelle2":loadImage("textures/UI/gameicons/PNG/Black/2x/trashcanOpen.png"),
        "home":loadImage("textures/UI/gameicons/PNG/Black/2x/home.png"),
        "audioon":loadImage("textures/UI/gameicons/PNG/Black/2x/audioOn.png"),
        "audiooff":loadImage("textures/UI/gameicons/PNG/Black/2x/audioOff.png"),
        "question":loadImage("textures/UI/gameicons/PNG/Black/2x/question.png"),
        "gamepad":loadImage("textures/UI/gameicons/PNG/Black/2x/gamepad.png"),
        "wrench":loadImage("textures/UI/gameicons/PNG/Black/2x/wrench.png"),
        "export":loadImage("textures/UI/gameicons/PNG/Black/2x/export.png"),
        "import":loadImage("textures/UI/gameicons/PNG/Black/2x/import.png"),
        "save":loadImage("textures/UI/gameicons/PNG/Black/2x/save.png")
      },
      "fond":loadImage("textures/Tiles/platformer-pack-redux-360-assets/PNG/Backgrounds/colored_grass.png"),
      "screenshot":loadImage("textures/Custom/screenshot.png"),
      "touchesClavier":loadImage("textures/Custom/Commande_clavier.jpg")
  };
/* ----- FIN TEXTURES ----- */

/* +++++ SONS +++++ */
var son = {
  player : {
    "ouvreInventaire":loadSound("textures/Custom/BagOpen1.mp3"),
    "fermeInventaire":loadSound("textures/Custom/BagClose1.mp3"),
    "marchePierre":loadSound("textures/Custom/footstep_concrete_walk_01.wav"),
    "tombePierre":loadSound("textures/Custom/footstep_concrete_land_01.wav"),
    "marcheTerre":loadSound("textures/Custom/footstep_sand_walk_01.wav"),
    "tombeTerre":loadSound("textures/Custom/footstep_sand_land_01.wav"),
    "marcheBois":loadSound("textures/Custom/footstep_wood_walk_01.wav"),
    "tombeBois":loadSound("textures/Custom/footstep_wood_land_01.wav"),
    "punch":loadSound("textures/Custom/kick_soft_jab_impact_05.wav")
  },
  "musique":loadSound("textures/Custom/music_loop.wav"),
  "clickBouton":loadSound("textures/Custom/Button7.mp3")
};

let jouerson = true;

/*
son.player.marcheTerre.volume = 0.25;
son.player.marchePierre.volume = 0.25;
son.player.marcheBois.volume = 0.25;
son.player.tombeTerre.volume = 0.25;
son.player.tombePierre.volume = 0.25;
son.player.tombeBois.volume = 0.25;

son.player.punch.volume = 0.25;

son.musique.volume = 0.25;
*/

/* ----- FIN SONS ----- */





// Police d'écriture
let pixelFont = loadFont("https://julienbrochard44.gitlab.io/terrascript/textures/VT323-Regular.ttf"); //loadFont("/textures/VT323-Regular.ttf");
console.log(pixelFont);

let p;
//let quantite = 0; // quantite de bloc que l'on monte ou descend à la fois du côté est
//let nquantite = 0; // quantite de bloc que l'on monte ou descend à la fois du côté ouest


let transitionB = 10;



let typeInventaire = 0;
let tailleInventaire;
let slotCraft = [34,35,38,36,37,39,40,41,42,33];






// New

let currentWorld = 0;
let currentDim = 0;

let localWorld = [
	
];