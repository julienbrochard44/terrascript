
let myCanvas = document.getElementById("terrascript");
myCanvas.width = window.innerWidth;
myCanvas.height = window.innerHeight;
var ctx = myCanvas.getContext("2d");



function loadImage(src) {
  let img = new Image();
  img.src = src;
  return img;
}

function image(img, x, y, width, height) {
  ctx.drawImage(img, x, y, width, height);
}

function fill(color) {
  ctx.fillStyle = color;
}

function noFill(){
  // pas d'intérieur
  // rgba("0,0,0,0") ?
}

function line(x1, y1, x2, y2) {
  ctx.beginPath();
  ctx.moveTo(x1, y1);
  ctx.lineTo(x2, y2);
  ctx.stroke(); 
}

function stroke(color) {
  ctx.strokeStyle = color;
}

function noStroke(){
  // pas de bordure
  // rgba("0,0,0,0") ?
}

function rect(x, y, width, height) {
  ctx.fillRect(x, y, width, height); 
}

function loadFont(url) {
  const font = new FontFace("pixelFont", "url(" + url + ")");
  font.load();
  return font;
}
//await font.load(); async function

function textFont(font, size, desc) {
  ctx.font = size + " " + font.family + " " + desc;
}

/*function textSize(size) {
  console.log("on m'appelle");
  let fontArgs = ctx.font.split(' ');
  let newSize = size + "px";
  ctx.font = newSize + ' ' + fontArgs[fontArgs.length - 1];
}*/

//let pixelFont = new FontFace('pixelFont', 'url(pixelfont.ttfqbikjdqs)');
//ctx.font = '48px pixelFont';

function text(txt, x, y) {
  ctx.strokeText(txt, x, y);
}

function translate(x, y) {
  ctx.translate(x, y);
}

// CALCULS POUR ROTATION DEPUIS LE CENTRE
function rotate(degrees) {
  ctx.rotate(degrees*Math.PI/180);
}


function resetMatrix() {
  ctx.resetTransform();
}

function loadSound(url) {
	return new Audio(url);
}

let repetition;
function frameRate(rate) {
  if(typeof rate == "number") {
    repetition = setInterval(draw,Math.floor(1000/rate));
  }
  else {
    return repetition; // TODO : compteur de fps
  }
}