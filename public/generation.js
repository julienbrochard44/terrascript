/* +++++ FONCTION CHUNK +++++ */
// la fonction chunk génère des valeurs numériques de hauteur du terrain en fonction des biomes changeants
function chunk(world, longueur, sens = "") {
    var nlength = world.dim[currentDim].T.nlength; // taille du tableau chunk du coté négatif
    var length = world.dim[currentDim].T.length; // taille du tableau chunk du coté positif
    // génération du coté ouest
    if (sens == "ouest") {
      for (var i = -nlength; i > -nlength - longueur; i--) {
        world.dim[currentDim].nbiomeL--;
        // changement des biomes
        if (world.dim[currentDim].nbiomeL <= 0) {
          world.dim[currentDim].B[i] = world.dim[currentDim].nbiomeS;
          world.dim[currentDim].nquantite = 0;
          world.dim[currentDim].nbiomeL = Math.floor(Math.random()*(biome[world.dim[currentDim].B[i]].longRdm)) + biome[world.dim[currentDim].B[i]].longMin;
          world.dim[currentDim].nbiomeS = Math.floor(Math.random()*(biome.length));
        } else {
          world.dim[currentDim].B[i] = world.dim[currentDim].B[i + 1];
        }
  
        world.dim[currentDim].B["nlength"]++;
        world.dim[currentDim].T["nlength"]++;
        if (world.dim[currentDim].nbiomeL <= transitionB) {
          if (world.dim[currentDim].T[i + 1] > biome[world.dim[currentDim].nbiomeS].max && world.dim[currentDim].nquantite >= 0) {
            world.dim[currentDim].nquantite = -1;
  
          } else if (world.dim[currentDim].T[i + 1] < biome[world.dim[currentDim].nbiomeS].min && world.dim[currentDim].nquantite <= 0) {
            world.dim[currentDim].nquantite = 1;
          }
        }
        // changement des hauteurs
        if (world.dim[currentDim].nquantite == 0) { // si le terrain est plat
          world.dim[currentDim].T[i] = world.dim[currentDim].T[i + 1];
          p = Math.random()*100;
          if (p <= biome[world.dim[currentDim].B[i]].probaPlusPlat && world.dim[currentDim].T[i] < biome[world.dim[currentDim].B[i]].max) {
            world.dim[currentDim].nquantite = 1;
          } else if (p >= 100 - biome[world.dim[currentDim].B[i]].probaPlusPlat && world.dim[currentDim].T[i] > biome[world.dim[currentDim].B[i]].min) {
            world.dim[currentDim].nquantite = -1;
          }
        } else if (world.dim[currentDim].nquantite > 0) { // si le terrain monte
          if (world.dim[currentDim].T[i + 1] + world.dim[currentDim].nquantite < biome[world.dim[currentDim].B[i]].max) {
            world.dim[currentDim].T[i] = world.dim[currentDim].T[i + 1] + world.dim[currentDim].nquantite;
  
            p = Math.random()*100;
            if (p <= biome[world.dim[currentDim].B[i]].probaChangementDrastique) {
              world.dim[currentDim].nquantite -= biome[world.dim[currentDim].B[i]].changementDirection;
            } else if (p >= 100 - biome[world.dim[currentDim].B[i]].probaPlusRigueux) {
              world.dim[currentDim].nquantite = 0;
            } else {
              p = Math.random()*101;
              if (p <= biome[world.dim[currentDim].B[i]].probaPlusRigueux && world.dim[currentDim].nquantite < biome[world.dim[currentDim].B[i]].maxq) {
                world.dim[currentDim].nquantite++;
              } else if (p >= 100 - biome[world.dim[currentDim].B[i]].probaPlusRigueux && world.dim[currentDim].nquantite > 1 && world.dim[currentDim].nquantite > -biome[world.dim[currentDim].B[i]].maxq) {
                world.dim[currentDim].nquantite--;
              }
            }
          } else {
            world.dim[currentDim].T[i] = biome[world.dim[currentDim].B[i]].max;
            world.dim[currentDim].nquantite = -1;
          }
  
        } else if (world.dim[currentDim].nquantite < 0) { // si le terrain descend
          if (world.dim[currentDim].T[i + 1] + world.dim[currentDim].nquantite > biome[world.dim[currentDim].B[i]].min) {
            world.dim[currentDim].T[i] = world.dim[currentDim].T[i + 1] + world.dim[currentDim].nquantite;
  
            p = Math.random()*101;
            if (p <= biome[world.dim[currentDim].B[i]].probaChangementDrastique) {
              world.dim[currentDim].nquantite += biome[world.dim[currentDim].B[i]].changementDirection;
            } else if (p >= 100 - biome[world.dim[currentDim].B[i]].probaPlusRigueux) {
              world.dim[currentDim].nquantite = 0;
            } else {
              p = Math.random()*101;
              if (p <= biome[world.dim[currentDim].B[i]].probaPlusRigueux && world.dim[currentDim].nquantite > -biome[world.dim[currentDim].B[i]].maxq) {
                world.dim[currentDim].nquantite--;
              } else if (p >= 100 - biome[world.dim[currentDim].B[i]].probaPlusRigueux && world.dim[currentDim].nquantite < -1 && world.dim[currentDim].nquantite < biome[world.dim[currentDim].B[i]].maxq) {
                world.dim[currentDim].nquantite++;
              }
            }
          } else {
            world.dim[currentDim].T[i] = biome[world.dim[currentDim].B[i]].min;
            world.dim[currentDim].nquantite = 1;
          }
  
        }
      }
    }
    // génération du côté est
    else if (sens == "est") {
      for (var i = length; i < length + longueur; i++) {
        world.dim[currentDim].biomeL--;
        // changement des biomes
        if (world.dim[currentDim].biomeL <= 0) {
          world.dim[currentDim].B[i] = world.dim[currentDim].biomeS;
          world.dim[currentDim].quantite = 0;
          world.dim[currentDim].biomeL = Math.floor(Math.random()*(biome[world.dim[currentDim].B[i]].longRdm)) + biome[world.dim[currentDim].B[i]].longMin;
          world.dim[currentDim].biomeS = Math.floor(Math.random()*(biome.length));
        } else {
          world.dim[currentDim].B[i] = world.dim[currentDim].B[i - 1];
        }
        if (world.dim[currentDim].biomeL <= transitionB) {
          if (world.dim[currentDim].T[i - 1] > biome[world.dim[currentDim].biomeS].max && world.dim[currentDim].quantite >= 0) {
            world.dim[currentDim].quantite = -1;
  
          } else if (world.dim[currentDim].T[i - 1] < biome[world.dim[currentDim].biomeS].min && world.dim[currentDim].quantite <= 0) {
            world.dim[currentDim].quantite = 1;
          }
        }
        // changement des hauteurs
        if (world.dim[currentDim].quantite == 0) { // si le terrain est plat
          world.dim[currentDim].T[world.dim[currentDim].T.length] = world.dim[currentDim].T[world.dim[currentDim].T.length - 1];
          p = Math.random()*100;
          if (p <= biome[world.dim[currentDim].B[i]].probaPlusPlat && world.dim[currentDim].T[i] < biome[world.dim[currentDim].B[i]].max) {
            world.dim[currentDim].quantite = 1;
          } else if (p >= 100 - biome[world.dim[currentDim].B[i]].probaPlusPlat && world.dim[currentDim].T[i] > biome[world.dim[currentDim].B[i]].min) {
            world.dim[currentDim].quantite = -1;
          }
        } else if (world.dim[currentDim].quantite > 0) { // si le terrain monte
          if (world.dim[currentDim].T[world.dim[currentDim].T.length - 1] + world.dim[currentDim].quantite < biome[world.dim[currentDim].B[i]].max) {
            world.dim[currentDim].T[world.dim[currentDim].T.length] = world.dim[currentDim].T[world.dim[currentDim].T.length - 1] + world.dim[currentDim].quantite;
  
            p = Math.random()*100;
            if (p <= biome[world.dim[currentDim].B[i]].probaChangementDrastique) {
              world.dim[currentDim].quantite -= biome[world.dim[currentDim].B[i]].changementDirection;
            } else if (p >= 100 - biome[world.dim[currentDim].B[i]].probaPlusRigueux) {
              world.dim[currentDim].quantite = 0;
            } else {
              p = Math.random()*101;
              if (p <= biome[world.dim[currentDim].B[i]].probaPlusRigueux && world.dim[currentDim].quantite < biome[world.dim[currentDim].B[i]].maxq) {
                world.dim[currentDim].quantite++;
              } else if (p >= 100 - biome[world.dim[currentDim].B[i]].probaPlusRigueux && world.dim[currentDim].quantite > 1 && world.dim[currentDim].quantite > -biome[world.dim[currentDim].B[i]].maxq) {
                world.dim[currentDim].quantite--;
              }
            }
          } else {
            world.dim[currentDim].T[world.dim[currentDim].T.length] = biome[world.dim[currentDim].B[i]].max;
            world.dim[currentDim].quantite = -1;
          }
        } else if (world.dim[currentDim].quantite < 0) { // si le terrain descend
          if (world.dim[currentDim].T[world.dim[currentDim].T.length - 1] + world.dim[currentDim].quantite > biome[world.dim[currentDim].B[i]].min) {
            world.dim[currentDim].T[world.dim[currentDim].T.length] = world.dim[currentDim].T[world.dim[currentDim].T.length - 1] + world.dim[currentDim].quantite;
  
            p = Math.random()*101;
            if (p <= biome[world.dim[currentDim].B[i]].probaChangementDrastique) {
              world.dim[currentDim].quantite += biome[world.dim[currentDim].B[i]].changementDirection;
            } else if (p >= 100 - biome[world.dim[currentDim].B[i]].probaPlusRigueux) {
              world.dim[currentDim].quantite = 0;
            } else {
              p = Math.random()*101;
              if (p <= biome[world.dim[currentDim].B[i]].probaPlusRigueux && world.dim[currentDim].quantite > -biome[world.dim[currentDim].B[i]].maxq) {
                world.dim[currentDim].quantite--;
              } else if (p >= 100 - biome[world.dim[currentDim].B[i]].probaPlusRigueux && world.dim[currentDim].quantite < -1 && world.dim[currentDim].quantite < biome[world.dim[currentDim].B[i]].maxq) {
                world.dim[currentDim].quantite++;
              }
            }
          } else {
            world.dim[currentDim].T[world.dim[currentDim].T.length] = biome[world.dim[currentDim].B[i]].min;
            world.dim[currentDim].quantite = 1;
          }
        }
      }
    } else { // si sens != ouest ou est
      chunk(world, longueur, "est");
      chunk(world, longueur, "ouest");
    }
  }
  /* +++++ FIN FONCTION CHUNK +++++ */
  
  // Hauteur de construction vide
  var ciel = 100;
  // Epaisseur de blocs max
  var sol = 100;
  
  // proba d'avoir une structure (fonctionne avec la fonction generation)
  var probaArbres = 50;

  
  
  // fonction exécuté une fois pour préparer le terrain
  function initialisation(world, sizex) {
    
    world.dim[currentDim].mapX += sizex;
  
    world.camera.posX = -Math.floor((sizex + 1) / 2) - Math.floor((pageX / cube - (sizex + 1)) / 2);
    world.camera.posY = Math.floor((world.dim[currentDim].T[0] * cube + ciel * cube - gameY / 2) / cube);
  
    /* chargement du point x = 0 */
    for (var y = 0; y < world.dim[currentDim].mapY; y++) {
      if (y >= world.dim[currentDim].T[0] + ciel) {
        if (y <= world.dim[currentDim].T[0] + ciel + biome[world.dim[currentDim].B[0]].c1[2] - 1) {
          world.dim[currentDim].map[y][0] = biome[world.dim[currentDim].B[0]].c1[0];
          world.dim[currentDim].dur[y][0] = objet[world.dim[currentDim].map[y][0]].dur;
          world.dim[currentDim].deco[y][0] = biome[world.dim[currentDim].B[0]].c1[1];
        } else if (y < world.dim[currentDim].T[0] + ciel + biome[world.dim[currentDim].B[0]].c2[2] + biome[world.dim[currentDim].B[0]].c1[2]) { // épaisseur terre
          world.dim[currentDim].map[y][0] = biome[world.dim[currentDim].B[0]].c2[0];
          world.dim[currentDim].dur[y][0] = objet[world.dim[currentDim].map[y][0]].dur;
          world.dim[currentDim].deco[y][0] = biome[world.dim[currentDim].B[0]].c2[1];
        } else if(y < world.dim[currentDim].T[0] + ciel /*+ biome[B[0]].c2[2] + biome[B[0]].c1[2]*/ + Math.floor(sol/2)) {
          if(Math.random()*10000 < 100){
            world.dim[currentDim].map[y][0] = "MF";
          }
          else if(Math.random()*10000 < 50){
            world.dim[currentDim].map[y][0] = "MO";
          }
          else if(Math.random()*10000 < 10){
            world.dim[currentDim].map[y][0] = "MD";
          }
          else {
            world.dim[currentDim].map[y][0] = "P";
          }
          world.dim[currentDim].dur[y][0] = objet[world.dim[currentDim].map[y][0]].dur;
          world.dim[currentDim].deco[y][0] = "P";
        }
        else {
          if(Math.random()*10000 < 10){
            world.dim[currentDim].map[y][0] = "MR";
          }
          else {
            world.dim[currentDim].map[y][0] = "R";
          }
          world.dim[currentDim].dur[y][0] = objet[world.dim[currentDim].map[y][0]].dur;
          world.dim[currentDim].deco[y][0] = "R";
        }
      } else {
        world.dim[currentDim].map[y][0] = " ";
        world.dim[currentDim].dur[y][0] = objet[world.dim[currentDim].map[y][0]].dur;
        world.dim[currentDim].deco[y][0] = " ";
      }
    }
    // herbe
    if(Math.random()*100 < biome[world.dim[currentDim].B[0]].pherbe){
      world.dim[currentDim].deco[world.dim[currentDim].T[0] + ciel-1][0] = biome[world.dim[currentDim].B[0]].herbe[Math.floor(Math.random()*(biome[world.dim[currentDim].B[0]].herbe.length))];
      world.dim[currentDim].dur[world.dim[currentDim].T[0] + ciel-1][0] = objet[world.dim[currentDim].deco[world.dim[currentDim].T[0] + ciel-1][0]].dur;
    }
    /* fin chargement du point 0 */
    
    // chargement de la map à une distance sizex dans le positif et négatif
    loadMap(world, sizex);
  
    // positionnement du player
    world.player.posY = (world.camera.posY + gameY / cube / 2) * cube - world.player.taille - 1; // position verticale à l'apparition
    world.player.posX = 0; // position horizontale à l'apparition
    
    
    if (typeof world.player.deplacement == "number") {
      nombreSaut = world.player.deplacement;
    } else {
      nombreSaut = 0;
    }
    
  
    //son.musique.loop = true;
    //son.musique.play();
   
  }
  
  // fonction utilisé pour charger une distance à gauche ou à droite en assossiant la fonction chunk et generation
  
  function loadMap(world, sizex, direction = "") {
    chunk(world, sizex, direction);
    generation(world, sizex, direction);
  }
  
  // fonction qui va utiliser les valeurs générées par chunk pour créer le monde
  function generation(world, sizex, direction = "") {
    // génération vers le côté est
    if (direction == "est") {
      // mise en place de blocs
      for (var x = world.dim[currentDim].T.length - sizex; x < world.dim[currentDim].T.length; x++) {
        for (var y = 0; y < world.dim[currentDim].mapY; y++) {
          if (y >= world.dim[currentDim].T[x] + ciel) {
            if (y <= world.dim[currentDim].T[x] + ciel + biome[world.dim[currentDim].B[x]].c1[2] - 1) {
              world.dim[currentDim].map[y][x] = biome[world.dim[currentDim].B[x]].c1[0];
              world.dim[currentDim].dur[y][x] = objet[world.dim[currentDim].map[y][x]].dur;
              world.dim[currentDim].deco[y][x] = biome[world.dim[currentDim].B[x]].c1[1];
            } else if (y < world.dim[currentDim].T[x] + ciel + biome[world.dim[currentDim].B[x]].c2[2] + biome[world.dim[currentDim].B[x]].c1[2]) { // épaisseur terre
              world.dim[currentDim].map[y][x] = biome[world.dim[currentDim].B[x]].c2[0];
              world.dim[currentDim].dur[y][x] = objet[world.dim[currentDim].map[y][x]].dur;
              world.dim[currentDim].deco[y][x] = biome[world.dim[currentDim].B[x]].c2[1];
            } else if(y < world.dim[currentDim].T[x] + ciel /*+ biome[B[x]].c2[2] + biome[B[x]].c1[2]*/ + Math.floor(sol/2)){
              if(Math.random()*10000 < 100){
                world.dim[currentDim].map[y][x] = "MF";
              }
              else if(Math.random()*10000 < 50){
                world.dim[currentDim].map[y][x] = "MO";
              }
              else if(Math.random()*10000 < 10){
                world.dim[currentDim].map[y][x] = "MD";
              }
              else {
                world.dim[currentDim].map[y][x] = "P";
              }
              world.dim[currentDim].dur[y][x] = objet[world.dim[currentDim].map[y][x]].dur;
              world.dim[currentDim].deco[y][x] = "P";
            }
            else {
              if(Math.random()*10000 < 10){
                world.dim[currentDim].map[y][x] = "MR";
              }
              else {
                world.dim[currentDim].map[y][x] = "R";
              }
              world.dim[currentDim].dur[y][x] = objet[world.dim[currentDim].map[y][x]].dur;
              world.dim[currentDim].deco[y][x] = "R";
            }
          } else {
            world.dim[currentDim].map[y][x] = " ";
            world.dim[currentDim].dur[y][x] = objet[world.dim[currentDim].map[y][x]].dur;
            world.dim[currentDim].deco[y][x] = " ";
          }
        }
        // herbe
        if(Math.random()*100 < biome[world.dim[currentDim].B[x]].pherbe){
          world.dim[currentDim].deco[world.dim[currentDim].T[x] + ciel-1][x] = biome[world.dim[currentDim].B[x]].herbe[Math.floor(Math.random()*(biome[world.dim[currentDim].B[x]].herbe.length))];
          world.dim[currentDim].dur[world.dim[currentDim].T[x] + ciel-1][x] = objet[world.dim[currentDim].deco[world.dim[currentDim].T[x] + ciel-1][x]].dur;
        }
      }
  
      // génération des structures (arbres)
      for (var x = world.dim[currentDim].T.length - sizex; x < world.dim[currentDim].T.length; x++) {
        if (Math.random()*100 <= probaArbres && biome[world.dim[currentDim].B[x]].structure.length > 0) {
          var rdm = Math.floor(Math.random()*(biome[world.dim[currentDim].B[x]].structure.length));
          if (x + biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length < world.dim[currentDim].T.length && world.dim[currentDim].B[x] == world.dim[currentDim].B[x+Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)+1]) { // vérifie si la structure a la place d'apparaitre et qu'elle ne change pas de biome
            x += Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)+1;
            for (var ya = 0; ya < biome[world.dim[currentDim].B[x]].structure[rdm].length; ya++) {
              for (var xa = 0; xa < biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length; xa++) {
                // poser blocs deco de la structure
                if (biome[world.dim[currentDim].B[x]].structure[rdm][ya][1][xa] != " "/* && (deco[T[x] + ciel+1 + ya - biome[B[x]].structure[rdm].length][x + xa - Math.floor(biome[B[x]].structure[rdm][0].length / 2)] == " " || deco[T[x] + ciel+1 + ya - biome[B[x]].structure[rdm].length][x + xa - Math.floor(biome[B[x]].structure[rdm][0].length / 2)] == "F")*/) {
                  if(biome[world.dim[currentDim].B[x]].structure[rdm][ya][1][xa] == "*"){ // si c'est une * , poser un bloc d'air
                    world.dim[currentDim].deco[T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)] = " ";
                  }
                  else{ // sinon poser le bloc
                    world.dim[currentDim].deco[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)] = biome[world.dim[currentDim].B[x]].structure[rdm][ya][1][xa];
                  }
                }
                // poser blocs map de la structure
                if (biome[world.dim[currentDim].B[x]].structure[rdm][ya][0][xa] != " "){
                  if(biome[world.dim[currentDim].B[x]].structure[rdm][ya][0][xa] == "*"){ // si c'est une * , poser un bloc d'air
                  world.dim[currentDim].map[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)] = " ";
                  }
                  else { // sinon poser le bloc
                    world.dim[currentDim].map[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)] = biome[world.dim[currentDim].B[x]].structure[rdm][ya][0][xa];
                  }
                }
                if(world.dim[currentDim].map[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)] != " "){
                  world.dim[currentDim].dur[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)] = objet[world.dim[currentDim].map[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)]].dur;
                }
                else if(world.dim[currentDim].deco[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)] != " ") {
                  world.dim[currentDim].dur[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)] = objet[world.dim[currentDim].deco[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)]].dur;
                }
                else {
                  world.dim[currentDim].dur[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)] = 0;
                }
              }
            }
            x += Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0].length / 2)+1;
          }
        }
      }
      // génération des structures (cave)
      for (var x = world.dim[currentDim].T.length - sizex; x < world.dim[currentDim].T.length; x++) {
        if (Math.random()*100 <= probaArbres && cave.length > 0) {
          var rdm = Math.floor(Math.random()*(cave.length));
          var profondeur = ciel + world.dim[currentDim].T[x] + 20 + Math.floor(Math.random()*(sol - world.dim[currentDim].T[x] - 20)) - cave[rdm].length;
          if (x + cave[rdm][0][0].length < world.dim[currentDim].T.length && world.dim[currentDim].B[x] == world.dim[currentDim].B[x+Math.floor(cave[rdm][0][0].length / 2)+1]) { // vérifie si la structure a la place d'apparaitre et qu'elle ne change pas de biome
            x += Math.floor(cave[rdm][0][0].length / 2)+1;
            for (var ya = 0; ya < cave[rdm].length; ya++) {
              for (var xa = 0; xa < cave[rdm][0][0].length; xa++) {
                // poser blocs deco de la structure
                if (cave[rdm][ya][1][xa] != " "/* && (deco[ya + profondeur][x + xa - Math.floor(cave[rdm][0].length / 2)] == " " || deco[ya + profondeur][x + xa - Math.floor(cave[rdm][0].length / 2)] == "F")*/) {
                  if(cave[rdm][ya][1][xa] == "*"){ // si c'est une * , poser un bloc d'air
                  world.dim[currentDim].deco[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)] = " ";
                  }
                  else{ // sinon poser le bloc
                    world.dim[currentDim].deco[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)] = cave[rdm][ya][1][xa];
                  }
                }
                // poser blocs map de la structure
                if (cave[rdm][ya][0][xa] != " "){
                  if(cave[rdm][ya][0][xa] == "*"){ // si c'est une * , poser un bloc d'air
                    world.dim[currentDim].map[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)] = " ";
                  }
                  else { // sinon poser le bloc
                    world.dim[currentDim].map[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)] = cave[rdm][ya][0][xa];
                  }
                }
                if(world.dim[currentDim].map[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)] != " "){
                  world.dim[currentDim].dur[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)] = objet[world.dim[currentDim].map[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)]].dur;
                }
                else if(world.dim[currentDim].deco[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)] != " ") {
                  world.dim[currentDim].dur[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)] = objet[world.dim[currentDim].deco[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)]].dur;
                }
                else {
                  world.dim[currentDim].dur[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)] = 0;
                }
              }
            }
            x += Math.floor(cave[rdm][0].length / 2)+1;
          }
        }
      }
    }
    // génération vers le côté ouest
    else if (direction == "ouest") {
      // mise en place des blocs
      for (var x = -world.dim[currentDim].T.nlength + sizex; x > -world.dim[currentDim].T.nlength; x--) {
        for (var y = 0; y < world.dim[currentDim].mapY; y++) {
          if (y >= world.dim[currentDim].T[x] + ciel) {
            if (y <= world.dim[currentDim].T[x] + ciel + biome[world.dim[currentDim].B[x]].c1[2] - 1) {
              world.dim[currentDim].map[y][x] = biome[world.dim[currentDim].B[x]].c1[0];
              world.dim[currentDim].dur[y][x] = objet[world.dim[currentDim].map[y][x]].dur;
              world.dim[currentDim].deco[y][x] = biome[world.dim[currentDim].B[x]].c1[1];
            } else if (y < world.dim[currentDim].T[x] + ciel + biome[world.dim[currentDim].B[x]].c2[2] + biome[world.dim[currentDim].B[x]].c1[2]) { // épaisseur terre
              world.dim[currentDim].map[y][x] = biome[world.dim[currentDim].B[x]].c2[0];
              world.dim[currentDim].dur[y][x] = objet[world.dim[currentDim].map[y][x]].dur;
              world.dim[currentDim].deco[y][x] = biome[world.dim[currentDim].B[x]].c2[1];
            } else if(y < world.dim[currentDim].T[x] + ciel /*+ biome[B[x]].c2[2] + biome[B[x]].c1[2]*/ + Math.floor(sol/2)) {
              if(Math.random()*10000 < 100){
                world.dim[currentDim].map[y][x] = "MF";
              }
              else if(Math.random()*10000 < 50){
                world.dim[currentDim].map[y][x] = "MO";
              }
              else if(Math.random()*10000 < 10){
                world.dim[currentDim].map[y][x] = "MD";
              }
              else {
                world.dim[currentDim].map[y][x] = "P";
              }
              world.dim[currentDim].dur[y][x] = objet[world.dim[currentDim].map[y][x]].dur;
              world.dim[currentDim].deco[y][x] = "P";
            }
            else {
              if(Math.random()*10000 < 10){
                world.dim[currentDim].map[y][x] = "MR";
              }
              else {
                world.dim[currentDim].map[y][x] = "R";
              }
              world.dim[currentDim].dur[y][x] = objet[world.dim[currentDim].map[y][x]].dur;
              world.dim[currentDim].deco[y][x] = "R";
            }
          } else {
            world.dim[currentDim].map[y][x] = " ";
            world.dim[currentDim].dur[y][x] = objet[world.dim[currentDim].map[y][x]].dur;
            world.dim[currentDim].deco[y][x] = " ";
          }
        }
        // herbe
        if(Math.random()*100 < biome[world.dim[currentDim].B[x]].pherbe){
          world.dim[currentDim].deco[world.dim[currentDim].T[x] + ciel-1][x] = biome[world.dim[currentDim].B[x]].herbe[Math.floor(Math.random()*(biome[world.dim[currentDim].B[x]].herbe.length))];
          world.dim[currentDim].dur[world.dim[currentDim].T[x] + ciel-1][x] = objet[world.dim[currentDim].deco[world.dim[currentDim].T[x] + ciel-1][x]].dur
        }
      }
  
      // génération de structures (arbres)
      for (var x = -world.dim[currentDim].T.nlength + sizex; x > -world.dim[currentDim].T.nlength; x--) {
        if (Math.random()*100 <= probaArbres && biome[world.dim[currentDim].B[x]].structure.length > 0) {
          var rdm = Math.floor(Math.random()*(biome[world.dim[currentDim].B[x]].structure.length));
          if (x - biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length > -world.dim[currentDim].T.nlength && world.dim[currentDim].B[x] == world.dim[currentDim].B[x-(Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)+1)]) { // vérifie si la structure a la place d'apparaitre et qu'elle ne change pas de biome
            x -= Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)+1;
            for (var ya = 0; ya < biome[world.dim[currentDim].B[x]].structure[rdm].length; ya++) {
              for (var xa = 0; xa < biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length; xa++) {
                // poser blocs deco de la structure
                if (biome[world.dim[currentDim].B[x]].structure[rdm][ya][1][xa] != " "/* && (deco[T[x] + ciel+1 + ya - biome[B[x]].structure[rdm].length][x + xa - Math.floor(biome[B[x]].structure[rdm][0].length / 2)] == " " || deco[T[x] + ciel+1 + ya - biome[B[x]].structure[rdm].length][x + xa - Math.floor(biome[B[x]].structure[rdm][0].length / 2)] == "F")*/) {
                  if(biome[world.dim[currentDim].B[x]].structure[rdm][ya][1][xa] == "*"){ // si c'est une * , poser un bloc d'air
                  world.dim[currentDim].deco[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)] = " ";
                  }
                  else{ // sinon poser le bloc
                    world.dim[currentDim].deco[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)] = biome[world.dim[currentDim].B[x]].structure[rdm][ya][1][xa];
                  }
                }
                // poser blocs map de la structure
                if (biome[world.dim[currentDim].B[x]].structure[rdm][ya][0][xa] != " "){
                  if(biome[world.dim[currentDim].B[x]].structure[rdm][ya][0][xa] == "*"){ // si c'est une * , poser un bloc d'air
                  world.dim[currentDim].map[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)] = " ";
                  }
                  else{ // sinon poser le bloc
                    world.dim[currentDim].map[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)] = biome[world.dim[currentDim].B[x]].structure[rdm][ya][0][xa];
                  }
                }
                if(world.dim[currentDim].map[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)] != " "){
                  world.dim[currentDim].dur[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)] = objet[world.dim[currentDim].map[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)]].dur;
                }
                else if(world.dim[currentDim].deco[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)] != " ") {
                  world.dim[currentDim].dur[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)] = objet[world.dim[currentDim].deco[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)]].dur;
                }
                else {
                  world.dim[currentDim].dur[world.dim[currentDim].T[x] + ciel+1 + ya - biome[world.dim[currentDim].B[x]].structure[rdm].length][x + xa - Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0][0].length / 2)] = 0;
                }
              }
            }
            x -= Math.floor(biome[world.dim[currentDim].B[x]].structure[rdm][0].length / 2)+1;
          }
        }
      }
      // génération de structures (cave)
      for (var x = -world.dim[currentDim].T.nlength + sizex; x > -world.dim[currentDim].T.nlength; x--) {
        if (Math.random()*100 <= probaArbres && cave.length > 0) {
          var rdm = Math.floor(Math.random()*(cave.length));
          var profondeur = ciel + world.dim[currentDim].T[x] + 20 + Math.floor(Math.random()*(sol - world.dim[currentDim].T[x] - 20)) - cave[rdm].length;
          if (x - cave[rdm][0][0].length > -world.dim[currentDim].T.nlength && world.dim[currentDim].B[x] == world.dim[currentDim].B[x-(Math.floor(cave[rdm][0][0].length / 2)+1)]) { // vérifie si la structure a la place d'apparaitre et qu'elle ne change pas de biome
            x -= Math.floor(cave[rdm][0][0].length / 2)+1;
            for (var ya = 0; ya < cave[rdm].length; ya++) {
              for (var xa = 0; xa < cave[rdm][0][0].length; xa++) {
                // poser blocs deco de la structure
                if (cave[rdm][ya][1][xa] != " "/* && (deco[ya + profondeur][x + xa - Math.floor(cave[rdm][0].length / 2)] == " " || deco[ya + profondeur][x + xa - Math.floor(cave[rdm][0].length / 2)] == "F")*/) {
                  if(cave[rdm][ya][1][xa] == "*"){ // si c'est une * , poser un bloc d'air
                  world.dim[currentDim].deco[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)] = " ";
                  }
                  else{ // sinon poser le bloc
                    world.dim[currentDim].deco[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)] = cave[rdm][ya][1][xa];
                  }
                }
                // poser blocs map de la structure
                if (cave[rdm][ya][0][xa] != " "){
                  if(cave[rdm][ya][0][xa] == "*"){ // si c'est une * , poser un bloc d'air
                    world.dim[currentDim].map[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)] = " ";
                  }
                  else{ // sinon poser le bloc
                    world.dim[currentDim].map[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)] = cave[rdm][ya][0][xa];
                  }
                }
                /* PROBLEME MISE EN PLACE DURETE GENERATION GROTTE A GAUCHE
                if(map[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)] != " "){
                  dur[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)] = objet[map[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)]].dur;
                }
                else if(deco[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)] != " ") {
                  dur[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)] = objet[deco[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)]].dur;
                }
                else {
                  dur[ya + profondeur][x + xa - Math.floor(cave[rdm][0][0].length / 2)] = 0;
                }
                */
              }
            }
            x -= Math.floor(cave[rdm][0].length / 2)+1;
          }
        }
      }
    } else { // si ni ouest ni est
      generation(world, sizex, "ouest");
      generation(world, sizex, "est");
    }
  }