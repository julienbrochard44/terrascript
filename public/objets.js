/* +++++ OBJETS: BLOCS, ITEMS, OUTILS +++++ */
let objet = [];

objet[" "] = {
    loot: " ",
    texture: undefined,
    texturef: undefined,
    dur: 0
  };
  
objet["T"] = {
    name: "Terre",
    loot: "T",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/dirt.png"),
    texturef: loadImage("textures/Custom/darkdirt.png"),
    type: "bloc",
    transparent: false,
    canFall: false,
    stack: 64,
    dur: 20,
    outil: "pelle",
    click: function() {}
};
  
objet["H"] = {
    name: "Herbe",
    loot: "T",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/dirt_grass.png"),
    texturef: "none",
    type: "bloc",
    transparent: false,
    canFall: false,
    stack: 64,
    dur: 20,
    outil: "pelle",
    click: function() {}
};
  
objet["HS"] = {
    name: "Herbe Ensablée",
    loot: "T",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/dirt_sand.png"),
    texturef: "none",
    type: "bloc",
    transparent: false,
    canFall: false,
    stack: 64,
    dur: 20,
    outil: "pelle",
    click: function() {}
};
  
objet["HN"] = {
    name: "Herbe Enneigée",
    loot: "T",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/dirt_snow.png"),
    texturef: "none",
    type: "bloc",
    transparent: false,
    canFall: false,
    stack: 64,
    dur: 20,
    outil: "pelle",
    click: function() {}
};
  
objet["P"] = {
    name: "Pierre",
    loot: "P",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/stone.png"),
    texturef: loadImage("textures/Custom/darkstone.png"),
    type: "bloc",
    transparent: false,
    canFall: false,
    stack: 64,
    dur: 100,
    outil: "pioche",
    click: function() {}
};
  
objet["MF"] = {
    name: "Minerais de fer",
    loot: "LF",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/stone_silver_alt.png"),
    texturef: "none",
    type: "bloc",
    transparent: false,
    canFall: false,
    stack: 64,
    dur: 150,
    outil: "pioche",
    click: function() {}
};
  
objet["LF"] = {
    name: "Lingot de Fer",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/ore_silver.png"),
    type: "item",
    stack: 64
};
  
objet["MO"] = {
    name: "Minerais d'Or",
    loot: "LO",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/stone_gold_alt.png"),
    texturef: "none",
    type: "bloc",
    transparent: false,
    canFall: false,
    stack: 64,
    dur: 200,
    outil: "pioche",
    click: function() {}
};
  
objet["LO"] = {
    name: "Lingot d'Or",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/ore_gold.png"),
    type: "item",
    stack: 64
};
  
objet["MD"] = {
    name: "Minerais de Diamant",
    loot: "LD",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/stone_diamond_alt.png"),
    texturef: "none",
    type: "bloc",
    transparent: false,
    canFall: false,
    stack: 64,
    dur: 500,
    outil: "pioche",
    click: function() {}
};
  
objet["LD"] = {
    name: "Lingot de Diamant",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/ore_diamond.png"),
    type: "item",
    stack: 64
};
  
objet["MR"] = {
    name: "Minerais de Rubis",
    loot: "LR",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/greystone_ruby_alt.png"),
    texturef: "none",
    type: "bloc",
    transparent: false,
    canFall: false,
    stack: 64,
    dur: 500,
    outil: "pioche",
    click: function() {}
};
  
objet["LR"] = {
    name: "Lingot de Rubis",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/ore_ruby.png"),
    type: "item",
    stack: 64
};
  
objet["PT"] = {
    name: "Pierre Taillée",
    loot: "PT",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/brick_grey.png"),
    texturef: loadImage("textures/Custom/darkbrick_grey.png"),
    type: "bloc",
    transparent: false,
    canFall: false,
    stack: 64,
    dur: 100,
    outil: "pioche",
    click: function() {}
};
  
objet["BT"] = {
    name: "Brique Taillée",
    loot: "BT",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/brick_red.png"),
    texturef: loadImage("textures/Custom/darkbrick_red.png"),
    type: "bloc",
    transparent: false,
    canFall: false,
    stack: 64,
    dur: 100,
    outil: "pioche",
    click: function() {}
};
  
objet["R"] = {
    name: "Roche",
    loot: "R",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/greystone.png"),
    texturef: loadImage("textures/Custom/darkgreystone.png"),
    type: "bloc",
    transparent: false,
    canFall: false,
    stack: 64,
    dur: 1000,
    outil: "pioche",
    click: function() {}
};
  
objet["S"] = {
    name: "Sable",
    loot: "S",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/sand.png"),
    texturef: "none",
    type: "bloc",
    transparent: false,
    canFall: true,
    stack: 16,
    dur: 15,
    outil: "pelle",
    click: function() {}
};
  
objet["GT"] = {
    name: "Gravier Terre",
    loot: "GT",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/gravel_dirt.png"),
    texturef: loadImage("textures/Custom/darkgravel_dirt.png"),
    type: "bloc",
    transparent: false,
    canFall: true,
    stack: 16,
    dur: 20,
    outil: "pelle",
    click: function() {}
};
  
objet["GP"] = {
    name: "Gravier Pierre",
    loot: "GP",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/gravel_stone.png"),
    texturef: loadImage("textures/Custom/darkgravel_stone.png"),
    type: "bloc",
    transparent: false,
    canFall: true,
    stack: 16,
    dur: 20,
    outil: "pioche",
    click: function() {}
};
  
objet["N"] = {
    name: "Neige",
    loot: "N",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/snow.png"),
    texturef: "none",
    type: "bloc",
    transparent: false,
    canFall: false,
    stack: 64,
    dur: 15,
    outil: "pelle",
    click: function() {}
};
  
objet["s"] = {
    name: "Grès",
    loot: "s",
    texture: loadImage("textures/Custom/sandstone.png"),
    texturef: loadImage("textures/Custom/darksandstone.png"),
    type: "bloc",
    transparent: false,
    canFall: false,
    stack: 64,
    dur: 50,
    outil: "pioche",
    click: function() {}
};
  
objet["B"] = {
    name: "Buche",
    loot: "B",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/trunk_top.png"),
    texturef: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/trunk_side.png"),
    type: "bloc",
    transparent: false,
    canFall: false,
    stack: 64,
    dur: 50,
    outil: "hache",
    click: function() {}
};
  
  
objet["p"] = {
    name: "Planche",
    loot: "p",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/wood.png"),
    texturef: "none",
    type: "bloc",
    transparent: false,
    canFall: false,
    stack: 64,
    dur: 50,
    outil: "hache",
    click: function() {}
};
  
objet["C"] = {
    name: "Cactus",
    loot: "C",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/cactus_top.png"),
    texturef: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/cactus_side.png"),
    type: "bloc",
    transparent: false,
    canFall: false,
    stack: 64,
    dur: 20,
    outil: "hache",
    click: function() {}
};
  
objet["F"] = {
    name: "Feuille",
    loot: "F",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/leaves_transparent.png"),
    texturef: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/leaves_transparent.png"),
    type: "bloc",
    transparent: true,
    canFall: false,
    stack: 64,
    dur: 5,
    outil: "hache",
    click: function() {}
};
  
objet["V"] = {
    name: "Verre",
    loot: " ",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/glass_frame.png"),
    texturef: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/glass.png"),
    type: "bloc",
    transparent: true,
    canFall: false,
    stack: 64,
    dur: 5,
    outil: " ",
    click: function() {}
};
  
objet["TdC"] = {
    name: "Table de Craft",
    loot: "TdC",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/table.png"),
    texturef: "none",
    type: "bloc",
    transparent: false,
    canFall: false,
    stack: 64,
    dur: 30,
    outil: "hache",
    click: function() {
      typeInventaire = 2;
      if(bouton[bouton.length-1].fonction != "crafting();"){
      boutons(730, gameY - 70 * 3 + 135, 50, 50, "crafting();", texture.inventaire.tan, texture.inventaire.brown, objet["TdC"].texture, 0.75);
      }
    }
};
  
objet["Fo"] = {
    name: "Four",
    loot: "Fo",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/oven.png"),
    texturef: "none",
    type: "bloc",
    transparent: false,
    canFall: false,
    stack: 64,
    dur: 75,
    outil: "pioche",
    click: function() {/*alert("four");*/}
};
  
objet["PiB"] = {
    name: "Pioche en Bois",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/pick_bronze.png"),
    type: "outil",
    stack: 1,
    mine: 3,
    outil: "pioche"
};
  
objet["HaB"] = {
    name: "Hache en Bois",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/axe_bronze.png"),
    type: "outil",
    stack: 1,
    mine: 3,
    outil: "hache"
};
  
objet["PeB"] = {
    name: "Pelle en Bois",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/shovel_bronze.png"),
    type: "outil",
    stack: 1,
    mine: 3,
    outil: "pelle"
};
  
objet["PiP"] = {
    name: "Pioche en Pierre",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/pick_iron.png"),
    type: "outil",
    stack: 1,
    mine: 9,
    outil: "pioche"
};
  
objet["HaP"] = {
    name: "Hache en Pierre",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/axe_iron.png"),
    type: "outil",
    stack: 1,
    mine: 9,
    outil: "hache"
};
  
objet["PeP"] = {
    name: "Pelle en Pierre",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/shovel_iron.png"),
    type: "outil",
    stack: 1,
    mine: 9,
    outil: "pelle"
};
  
objet["PiF"] = {
    name: "Pioche en Fer",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/pick_silver.png"),
    type: "outil",
    stack: 1,
    mine: 18,
    outil: "pioche"
};
  
objet["HaF"] = {
    name: "Hache en Fer",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/axe_silver.png"),
    type: "outil",
    stack: 1,
    mine: 18,
    outil: "hache"
};
  
objet["PeF"] = {
    name: "Pelle en Fer",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/shovel_silver.png"),
    type: "outil",
    stack: 1,
    mine: 18,
    outil: "pelle"
};
  
objet["PiO"] = {
    name: "Pioche en Or",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/pick_gold.png"),
    type: "outil",
    stack: 1,
    mine: 20,
    outil: "pioche"
};
  
objet["HaO"] = {
    name: "Hache en Or",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/axe_gold.png"),
    type: "outil",
    stack: 1,
    mine: 20,
    outil: "hache"
};
  
objet["PeO"] = {
    name: "Pelle en Or",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/shovel_gold.png"),
    type: "outil",
    stack: 1,
    mine: 20,
    outil: "pelle"
};
  
objet["PiD"] = {
    name: "Pioche en Diamant",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/pick_diamond.png"),
    type: "outil",
    stack: 1,
    mine: 40,
    outil: "pioche"
};
  
objet["HaD"] = {
    name: "Hache en Diamant",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/axe_diamond.png"),
    type: "outil",
    stack: 1,
    mine: 40,
    outil: "hache"
};
  
objet["PeD"] = {
    name: "Pelle en Diamant",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/shovel_diamond.png"),
    type: "outil",
    stack: 1,
    mine: 40,
    outil: "pelle"
};
  
objet["Cha"] = {
    name: "Charbon",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Items/ore_coal.png"),
    type: "item",
    stack: 64
};
  
objet["H1"] = {
    name: "Herbe1",
    loot: " ",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/grass1.png"),
    texturef: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/grass1.png"),
    type: "bloc",
    transparent: true,
    canFall: true,
    stack: 64,
    dur: 5,
    outil: "hache",
    click: function() {}
};
  
objet["H2"] = {
    name: "Herbe2",
    loot: " ",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/grass2.png"),
    texturef: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/grass2.png"),
    type: "bloc",
    transparent: true,
    canFall: true,
    stack: 64,
    dur: 5,
    outil: "hache",
    click: function() {}
};
  
objet["H3"] = {
    name: "Herbe3",
    loot: " ",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/grass3.png"),
    texturef: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/grass3.png"),
    type: "bloc",
    transparent: true,
    canFall: true,
    stack: 64,
    dur: 5,
    outil: "hache",
    click: function() {}
};
  
objet["H4"] = {
    name: "Herbe4",
    loot: " ",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/grass4.png"),
    texturef: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/grass4.png"),
    type: "bloc",
    transparent: true,
    canFall: true,
    stack: 64,
    dur: 5,
    outil: "hache",
    click: function() {}
};
  
objet["H5"] = {
    name: "Herbe5",
    loot: " ",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/grass_tan.png"),
    texturef: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/grass_tan.png"),
    type: "bloc",
    transparent: true,
    canFall: true,
    stack: 64,
    dur: 5,
    outil: "hache",
    click: function() {}
};
  
objet["H6"] = {
    name: "Herbe6",
    loot: " ",
    texture: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/grass_brown.png"),
    texturef: loadImage("textures/Tiles/voxel-pack/PNG/Tiles/grass_brown.png"),
    type: "bloc",
    transparent: true,
    canFall: true,
    stack: 64,
    dur: 5,
    outil: "hache",
    click: function() {}
};

/* ----- FIN OBJETS ----- */